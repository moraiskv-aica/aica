% ***************************************************************
%% digital_draw.sty
%% Copyright 2023 Kevin Paula Morais
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   https://www.latex-project.org/lppl.txt
% and version 1.3c or later is part of all distributions of LaTeX
% version 2008 or later.
%
% This work has the LPPL maintenance status `maintained'.
% 
% The Current Maintainer of this work is Kevin Paula Morais.
%
% This work consists of the files digital_draw.sty
% ***************************************************************

% ***************************************************************
%      Author: Kevin Paula Morais
%        Date: 20/11/2023
%        File: digital_draw.tex
% Description: Useful commands to draw data registers
% ***************************************************************
\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{digital_draw}

\RequirePackage{tikz}
\RequirePackage{tikz-timing}

% renew this commands to use another text
\newcommand{\pkgTextRegisterLabel}   {Register}
\newcommand{\pkgTextRegisterAddress} {Address}
\newcommand{\pkgTextRegisterReset}   {Reset}
\newcommand{\pkgTextRegisterAccess}  {Access}
\newcommand{\pkgTextRegisterROAccess}{R}
\newcommand{\pkgTextRegisterWOAccess}{W}
\newcommand{\pkgTextRegisterRWAccess}{R/W}

% this commands are for internal use only
\newcommand{\pkg@registerSize}{7}
\newcommand{\pkg@registerOffset}{0}
\newcommand{\pkg@memLastY}{0}
\newcommand{\pkg@memStartX}{0.25\textwidth}
\newcommand{\pkg@memWidthX}{0.35\textwidth}

% ******************************************************************************
% environment to draw a register
% ******************************************************************************
% 1= figure position (optional)
% 2= register address
% 3= register reset value
\newenvironment{register-figure}[4][htb]
{
    \begin{figure}[#1]
        \begin{tabular}{r l}
            \textbf{\pkgTextRegisterLabel}   & \texttt{#2}\\
            \textbf{\pkgTextRegisterAddress} & \texttt{#3}\\
            \textbf{\pkgTextRegisterReset}   & \texttt{#4}\\
        \end{tabular}

        \centering
}{  \end{figure}
}

% ******************
% Example of use: creating a register of 8 bits
% \begin{register-figure}{\adcAddrControle}{0x0000\_0000}
%     \drawRegister{8}{0}{
%         \resbits{7}{7}{res}
%         \rwbits{6}{5}{SA}
%         \rwbits{4}{3}{MC}
%         \rwbits{2}{1}{OP}
%         \rwbits{0}{0}{EX}
%     }
%     \caption{Registrador de controle do Conversor A/D}
% \end{register-figure}

% ******************
% Example of use: creating a register of 32 bits, with 2 of 16 bits
%           Nota: \drawRegister tem como param o tamanho do registrador, e segundo seu bit inicial
%                  por isso criamos um registrador de 32 bits com 2 de 16, um começando em 0
%                  e outro em 16.
% \begin{register-figure}{\adcAddrControle}{0x0000\_0000}
%     \drawRegister{16}{16}{
%         \resbits{31}{16}{res}
%     }
%     \drawRegister{16}{0}{
%         \resbits{15}{7}{res}
%         \rwbits{6}{5}{SA}
%         \rwbits{4}{3}{MC}
%         \rwbits{2}{1}{OP}
%         \rwbits{0}{0}{EX}
%     }
%     \caption{Registrador de controle do Conversor A/D}
% \end{register-figure}

% ******************************************************************************
% commands to draw registers
% ******************************************************************************
\pgfdeclarelayer{background}
\pgfdeclarelayer{foreground}
\pgfsetlayers{background,main,foreground}

% 1= register size
% 2= size offset (used to draw a 16bits register which represents bits 31:16, for example)
% 3= bits command
\newcommand{\drawRegister}[3]{
    \begin{tikzpicture}[scale=0.92]
        % set command which will define the current register offset
        \renewcommand{\pkg@registerSize}{#1}
        \renewcommand{\pkg@registerOffset}{#2}

        \begin{pgfonlayer}{foreground}
            \draw [thick] (0,0) rectangle (#1,0.7);
            \pgfmathsetmacro\result{#1-1}
            % add bit separator
            % \foreach \x in {1,...,\result}
            %     \draw [thick] (\x,0.7) -- (\x, 0.6);
        \end{pgfonlayer}
        \node [below left, align=right] at (0,0) {\footnotesize \pkgTextRegisterAccess};
        #3
    \end{tikzpicture}
}

% ******************************************************************************
% Commands used to draw a bit field with specific access
% 1= lsb
% 2= msb
% 3= field name
\newcommand{\rwbits}[3]{
    \pkg@bitfield{#1}{#2}{#3}{\pkgTextRegisterRWAccess}{fill=white}
}
\newcommand{\robits}[3]{
    \pkg@bitfield{#1}{#2}{#3}{\pkgTextRegisterROAccess}{fill=white}
}
\newcommand{\wobits}[3]{
    \pkg@bitfield{#1}{#2}{#3}{\pkgTextRegisterWOAccess}{fill=white}
}
\newcommand{\resbits}[3]{
    \pkg@bitfield{#1}{#2}{#3}{\pkgTextRegisterROAccess}{fill=lightgray}
}

% ******************************************************************************
% 1= lsb
% 2= msb
% 3= field name
% 4= type of access
% 5= fill color
\newcommand{\pkg@bitfield}[5]{
    \pgfmathsetmacro\msbitRaw{#1-\pkg@registerOffset}
    \pgfmathsetmacro\lsbitRaw{#2-\pkg@registerOffset}

    % set coordinates
    \pgfmathsetmacro\msbitpos{\pkg@registerSize - \msbitRaw - 1}
    \pgfmathsetmacro\lsbpos{\pkg@registerSize - \lsbitRaw}
    \pgfmathsetmacro\fieldpos{\pkg@registerSize - \msbitRaw + ((\msbitRaw-\lsbitRaw)/2) -0.5}

    % draw field rectangle
    \draw [thick, #5] (\msbitpos,0) rectangle (\lsbpos,0.7) node[pos=0.5]{\footnotesize #3};

    % draw access type
    \node [below, align=center] at (\fieldpos, 0) {\footnotesize #4};

    % draw bit numbers
    \pgfmathsetmacro\mslabel{#1}
    \pgfmathsetmacro\lslabel{#2}
    \node [above, align=center] at (\msbitpos+0.5, 0.75) {\pgfmathprintnumber{\mslabel}};
    \node [above, align=center] at (\lsbpos-0.5, 0.75) {\pgfmathprintnumber{\lslabel}};
}

% ******************************************************************************
% command + environment to draw a waveform
% ******************************************************************************
\newcommand{\pkgAddWaveFormColumn}[1]{
  \extracode
  \begin{pgfonlayer}{background}
      \begin{scope}[semitransparent ,thick]
          \vertlines[darkgray,dotted]{#1}
      \end{scope}
  \end{pgfonlayer}
}

% 1= x scaleposition
% 2= x scale
% 3= y scale
\newenvironment{digital-waveform}[1][htb]
{
    \begin{figure}[#1]
        % \caption{\label{fig_met_cache_instr_flush}Diagrama de um flush no núcleo, afetando a cache de instrução.}
        \centering
        \begin{tikztimingtable}[xscale=6,yscale=2]

            % ****************
            % example of text to be used inside the environment
            % clock		&	[c]10{c}	\\
            % flush		&	LLHLL	\\
            % PC			&	D{14}D{18}D{22}D{40}D{44}	\\
            % Instr. BUS	&	D{MEM[8]}D{MEM[14]}D{[red]MEM[18]}D{[red]MEM[22]}D{MEM[40]}	\\
            % ****************
}{
            \pkgAddWaveFormColumn
            % ****************
        \end{tikztimingtable}
        % ****************
        \vspace{\baselineskip}
    \end{figure}
}

% ******************************************************************************
% environment to a memory map
% ******************************************************************************
% This environment should be used to drawn multiples rectangles/blocks of memory
% in sequence, thus, drawing a memory map outside this environment the placement
% of the blocks isn't guarantted, since it wont keep track correctly of the last
% block position.
% The input order of blocks, through the command \memMapAddress, must be from
% the higher to the lower address
% 
% 1=figure position
% 2=sequence of commands \memMapAddress and \memMapSectionLine, which are used
% here as an input so we can put the caption just after the tikzpicture
\newenvironment{mem-figure}[2][htb]
{
    \renewcommand{\pkg@memLastY}{0} %resets value
    \begin{figure}[#1]
        \centering
        \begin{tikzpicture}
        #2
        \end{tikzpicture}
}{
    \end{figure}
    \renewcommand{\pkg@memLastY}{0} %resets value
}

% 1=rectangle height (optional, defaults to 1)
% 2=lower address
% 3=higher address
% 4=rectangle text
% 5=rectangle fill color
\newcommand{\memMapAddress}[5][1]{
    \filldraw[fill=#5, draw=black, thick, fill opacity=1] (\pkg@memStartX, \pkg@memLastY) rectangle ++(\pkg@memWidthX, -#1);

    \node[right] at (\pkg@memStartX+\pkg@memWidthX, \pkg@memLastY-0.2)    {\texttt{#2}};
    \node[right] at (\pkg@memStartX+\pkg@memWidthX, \pkg@memLastY-#1+0.2) {\texttt{#3}};
    \node[align=center, text width=\pkg@memWidthX] at (\pkg@memStartX+ \pkg@memWidthX/2, \pkg@memLastY-#1/2){\textbf{#4}};

    \pgfmathsetmacro{\pkg@memLastY}{\pkg@memLastY-#1}
}

% draws a horizontally dashed line at the last drawn rectangle lower address, to the right, used as separator
% between blocks of memory
\newcommand{\memMapSectionLine}{
    \draw[gray, dashed] (\pkg@memStartX+\pkg@memWidthX, \pkg@memLastY) -- ++(0.1\textwidth, 0);
}

% Example:
% \begin{mem-figure}{
%     \memMapAddress[3]{0x1C01\_0000}{0x1C07\_FFFF}{Memória L2 Compartilhada (512 kiB RAM)}{blue!30!green!30}
%     \memMapSectionLine
%     \memMapAddress[1]{0x1C00\_0000}{0x1C00\_FFFF}{Memória L2 Privada (64 kiB RAM)}{blue!30!green!30}
%     \memMapAddress[1.5]{}{}{}{gray!40}
%     \memMapSectionLine
%     \memMapAddress{0x1A11\_0000}{0x1A11\_????}{Debug Unit}{white}
% }
%     \caption{Mapa de Memória com endereços iniciais e finais das principais seções.}
%     \label{fig:ch:mem_map:map}
% \end{mem-figure}
