% ***************************************************************
%% project.cls
%% Copyright 2023 Kevin Paula Morais
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   https://www.latex-project.org/lppl.txt
% and version 1.3c or later is part of all distributions of LaTeX
% version 2008 or later.
%
% This work has the LPPL maintenance status `maintained'.
% 
% The Current Maintainer of this work is Kevin Paula Morais.
%
% This work consists of the files project.cls
% ***************************************************************

% ***************************************************************
%      Author: Kevin Paula Morais
%        Date: 25/08/2023
%        File: project.cls
% Description: Main class used by this project template
% ***************************************************************
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{project}
\LoadClass[12pt,a4paper]{report}

\usepackage[brazil]{babel}

\RequirePackage{appendix}
\RequirePackage{graphicx}
\RequirePackage{multicol}
\RequirePackage{setspace}
\RequirePackage{xcolor}

% set fonts
\usepackage[scaled]{helvet}

\usepackage{hyperref}   % used to configure references
\usepackage{calc}
\usepackage{eso-pic}    % used to configure pages background
\usepackage{fancyhdr}   % used to configure header/footer layout
\usepackage{listings}   % used to insert code snippets
\usepackage{tikz}       % used to draw frames

% ***************************************************************
% Custom Packages
% ***************************************************************
\usepackage{class/digital_draw}

% ***************************************************************
% Colors
% ***************************************************************
% classic blue and green colors
\definecolor{clsclsDarkBlue}{RGB}{0,104,203}
\definecolor{clsBlue}       {RGB}{0,167,253}
\definecolor{clsDarkGray}   {RGB}{34,41,51}
\definecolor{clsGray}       {RGB}{75,89,110}
\definecolor{clsNeonGreen}  {RGB}{40,216,211}

% ***************************************************************
% Language setup
% ***************************************************************
\newcommand{\ifPTBR}[1]{
  \iflanguage{brazil}{#1}{}
}

\ifPTBR{\typeout{[Build] Language: Brazil}}

% these ones should not be needed if babel is set to brazil
% \ifPTBR{\renewcommand{\tableautorefname}  {Tabela}}
% \ifPTBR{\renewcommand{\figureautorefname} {Figura}}
% \ifPTBR{\renewcommand{\chapterautorefname}{Capítulo}}
% \ifPTBR{\renewcommand{\sectionautorefname}{Seção}}
% \ifPTBR{\renewcommand{\equationautorefname}{Equação}}
% \ifPTBR{\renewcommand{\contentsname}}     {Sumário}
% \ifPTBR{\renewcommand{\listfigurename}}   {Lista de Figuras}

% set registers text to pt-br
\ifPTBR{
  \renewcommand{\pkgTextRegisterLabel}   {Registrador}
  \renewcommand{\pkgTextRegisterAddress} {Endereço}
  \renewcommand{\pkgTextRegisterReset}   {Reset}
  \renewcommand{\pkgTextRegisterAccess}  {Acesso}
  \renewcommand{\pkgTextRegisterROAccess}{L}
  \renewcommand{\pkgTextRegisterWOAccess}{E}
  \renewcommand{\pkgTextRegisterRWAccess}{L/E}
}

% ***************************************************************
% Hyperref Setup
% ***************************************************************
\hypersetup{
  colorlinks=true,
  filecolor=red,
  urlcolor=clsBlue
}

% ***************************************************************
% Listings Setup - used to insert code snippets
% ***************************************************************
\definecolor{colorCodePurple}{rgb}{0.58,0,0.82}
\definecolor{colorCodeComment}{RGB}{42, 163, 97}
\colorlet{codeBackgroundColor}{clsGray!10}
\usepackage{listings}
\lstdefinestyle{customListingStyle}{
  backgroundcolor=\color{codeBackgroundColor},
  numberstyle=\color{clsBlue},
  commentstyle=\color{colorCodeComment},
  keywordstyle=\bfseries\color{clsBlue},
  identifierstyle=\color{black},
  stringstyle=\itshape\color{magenta},
  basicstyle=\ttfamily\footnotesize,
  breakatwhitespace=false,
  breaklines=true,
  captionpos=b,
  keepspaces=true,
  numbers=left,
  numbersep=5pt,
  showspaces=false,
  showstringspaces=false,
  showtabs=false,
  tabsize=2
}
\lstset{style=customListingStyle}

% ***************************************************************
% Page Layout
% ***************************************************************
% page size
\setlength{\paperheight}{297mm}
\setlength{\paperwidth} {210mm}

% horizontal setup
\setlength {\hoffset}       {-0.04cm} % dec 0.04cm from default 1in hoffset
\setlength {\oddsidemargin} {0cm}     %{3cm}
\setlength {\evensidemargin}{0cm}     %{3cm}
\setlength {\textwidth}     {16cm}    % \paperwidth - 5cm
\setlength {\marginparsep}  {0mm}
\setlength {\marginparwidth}{0mm}
\setlength {\marginparpush} {0mm}
\setlength {\marginparwidth}{0mm}

% vertical setup
\setlength {\voffset}   {0cm}     % must be 0 to simplify use of eso-pic
\setlength {\topmargin} {0cm}
\setlength {\headheight}{0cm}
\setlength {\headsep}   {0cm}
\setlength {\textheight}{23.65cm}
\setlength {\footskip}  {2cm}

% header and footer layout
\pagestyle{fancy}
\fancyhf{}

\fancyfoot[R]{\thepage}                   % pagenumber at lower right
\fancyfoot[L]{\nouppercase{\leftmark}}    % current chapter at lower left

\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{1pt}
\renewcommand{\headwidth}    {\textwidth}

% ***************************************************************
% Page background picture
% ***************************************************************
% tikz frame, there is no need to shift {xy} since eso-pic defaults
% origin to the lower left corner as (0,0), otherwise origin would
% be at the higher left corner (\clsHOffset+\parindent,\paperheight)
\newcommand{\cls@verticalPageFrame}[2]{%
  \begin{tikzpicture}[remember picture, overlay]
  % \begin{scope}[shift={(-\clsHOffset-\parindent,\clsVOffset)}]
  \shade[left color=clsBlue,  right color=#1, line width=0cm]
        (0,   0) rectangle (1cm, \paperheight);
  \shade[left color=clsDarkGray, right color=#2, line width=0cm]
        (0.99cm, 0) rectangle (1.15cm, \paperheight);
  \end{tikzpicture}%
}

\newcommand{\cls@horizontalPageFrame}[2]{%
  \begin{tikzpicture}[remember picture, overlay]
  % \begin{scope}[shift={(-\clsHOffset-\parindent,\clsVOffset)}]
  \shade[left color=clsBlue,  right color=#1, line width=0cm]
        (0,   0) rectangle (\paperwidth, 1cm);
  \shade[left color=clsDarkGray, right color=#2, line width=0cm]
        (0, 0.99cm) rectangle (\paperwidth, 1.15cm);
  \end{tikzpicture}%
}

% add frame to each page
\newcommand{\clsAddVerticalColorFrame}[2]{
  \AddToShipoutPictureBG{\cls@verticalPageFrame{#1}{#2}}
}
\newcommand{\clsAddHorizontalColorFrame}[2]{
  \AddToShipoutPictureBG{\cls@horizontalPageFrame{#1}{#2}}
}

% ***************************************************************
% Set format for chapters, sections, and so it goes
% ***************************************************************
% sections enabled: chapters(1); section(2)
% \setcounter{secnumdepth}{2}

% chapter
\newcommand{\cls@chapterFormat}{
  \@startsection
    {chapter}
    {1}
    {\z@}
    {-3.5ex \@plus -1ex \@minus -.2ex}
    {0.1\baselineskip} % {2\baselineskip}
    {\LARGE\bfseries}
}

% section
\newcommand{\cls@sectionFormat}{
  \vspace{2\baselineskip}
  \@startsection
    {section}
    {1}
    {\z@}
    {-3.5ex \@plus -1ex \@minus -.2ex}
    {2.3ex \@plus.2ex}
    {\large\bfseries}
}

% subsection
\newcommand{\cls@subSectionFormat}{
  \vspace{1.5\baselineskip}
  \@startsection
    {subsection}
    {2}
    {\z@}
    {-3.25ex \@plus -1ex \@minus -.2ex}
    {1.5ex \@plus .2ex}
    {\normalsize\bfseries}
}

% subsubsection
\newcommand{\cls@subSubSectionFormat}{
  \vspace{1.5\baselineskip}
  \@startsection
    {subsubsection}
    {3}
    {\z@}
    {-3.25ex \@plus -1ex \@minus -.2ex}
    {1.5ex \@plus .2ex}
    {\normalsize\bfseries\it}
}

% renew
\renewcommand{\chapter}      {\cls@chapterFormat}
\renewcommand{\section}      {\cls@sectionFormat}
\renewcommand{\subsection}   {\cls@subSectionFormat}
\renewcommand{\subsubsection}{\cls@subSubSectionFormat}

% insert text with reference as 'Seção <number>'
\newcommand{\clsRefSection}[1]{
  \hyperref[#1]{Seção \ref{#1}}
}

% chapter command with extra layout
\newcommand{\clsChapter}[1]{
  \clearpage  %starts at a new page
  \chapter{#1}

  % draw underline
  \begin{tikzpicture}
    % \draw [very thick]  (0, 0) -- (\textwidth, 0);
    \draw (0, 0) -- (\textwidth, 0);
  \end{tikzpicture}

  % page without pagenumber+head+footer
  \thispagestyle{empty}

  % close 2\baselineskip when added to the \chapter definition
  \vspace{1.9\baselineskip}
}

% ***************************************************************
% NewCommands - Info
% ***************************************************************
\newcommand{\clsDocTitle}    {Define the title with \textbf{renewcommand{clsDocTitle}{<title>}}}
\newcommand{\clsDocMiniTitle}{Define the mini title with \textbf{renewcommand{clsDocMiniTitle}{<minititle>}}}
\newcommand{\clsVersion}     {Define the version with \textbf{renewcommand{clsVersion}{<version>}}}
\newcommand{\clsVersionDate} {\today}

% ***************************************************************
% NewCommands - Insert text
% ***************************************************************
% insert a snippet file with std vspace before
\newcommand{\clsInsertSnippetFile}[1]{
  \vspace{1\baselineskip}
  \lstinputlisting{#1}
}

% simple url insert as small with link
\newcommand{\clsURL}[1]{
  {\small
  \url{#1}
  }
}

% custom reference without link to
% a path (any type of path, winregistry, filesystem, etc.)
\newcommand{\clsRefPath}[1]{
  {\small
  \color{magenta}
  #1
  }
}

% ***************************************************************
% Front page
% ***************************************************************
\newcommand{\cls@createFront}{
  \begin{center}
    \hspace{1cm}\vspace{4cm}

    % logo
    \ifdefined\clsCreateLogo
      \begin{figure}[htb]
        \centering
        \includegraphics[width=0.25\textwidth]{class/logo.png}
      \end{figure}
    \fi

    % document title
    {\Large{\textbf{\clsDocTitle}}}\\
    \vspace{0.5\baselineskip}
    {v\clsVersion}\\

    % info
    \vspace{\fill}
    {\clsVersionDate}
  \end{center}

  % remove style from page (like page number), and start a new one
  \thispagestyle{empty}
  \clearpage
}
  
% ***************************************************************
% Summary
% ***************************************************************
% chapter/sections summary
\newcommand{\cls@createSummary}{
  \tableofcontents
  % end page
  \thispagestyle{empty}
  \clearpage
}

% list of figures
\newcommand{\cls@createFigureSummary}{
  \listoffigures
  % end page
  \thispagestyle{empty}
  \clearpage
}

% ***************************************************************
% PreTextual Command
% ***************************************************************
\newcommand{\clsPretext}{

  % create front page
  \cls@createFront

  % create summary page
  \cls@createSummary

  % create figure summary page (conditional)
  \ifdefined\clsCreateListOfFigures
    \cls@createFigureSummary
  \fi

  % reset sett before document
  \normalsize
}
