library ieee;
use ieee.std_logic_1164.all;

use work.core_pkg.all;
use work.riscv_pkg.all;

-- *****************************************************************************
entity core_stage2_id is
  port(
    rst_i           : in  std_logic;
    clk_i           : in  std_logic;
    -- stage stall/flush flag
    flush_i         : in  std_logic;
    stall_i         : in  std_logic;
    bubble_o        : out std_logic;
    -- signals from stage EX and WB
    ex2id_i         : in  t_ex2id;
    wb2id_i         : in  t_wb2id;
    -- stage pipeline register, IF/ID and ID/EX
    pipe_id_i       : in  t_pipe_id;
    pipe_ex_o       : out t_pipe_ex
  );
end entity core_stage2_id;

-- *****************************************************************************
architecture behavior of core_stage2_id is

  signal r_pipe_ex : t_pipe_ex;

  -- Instruction decoded (RVC instructions are extended to it's 32 bits equivalent)
  signal instr_decoded : std_logic_vector(31 downto 0);

  -- Instruction fields
  alias instr_rs2    : std_logic_vector(N_XLEN-1 downto 0) is instr_decoded(24 downto 20);
  alias instr_funct3 : std_logic_vector(2 downto 0)        is instr_decoded(14 downto 12);
  alias instr_rd     : std_logic_vector(N_XLEN-1 downto 0) is instr_decoded(11 downto 7);

  -- Control unit output
  signal ctrl_regfile_write : std_logic;
  signal ctrl_regfile_sel0  : std_logic;
  signal ctrl_branch_unc    : std_logic;
  signal ctrl_branch        : std_logic;
  signal ctrl_branch_sel    : std_logic;
  signal ctrl_mread         : std_logic;
  signal ctrl_mwrite        : std_logic;
  signal ctrl_csr_en        : std_logic;
  signal ctrl_pc_arg_sel    : std_logic;
  signal ctrl_alu_op        : std_logic_vector(1 downto 0);
  signal ctrl_alu_sela      : std_logic;
  signal ctrl_alu_selb      : std_logic;
  signal ctrl_excp_instr    : std_logic;
  signal ctrl_imm           : std_logic_vector(N_DATA-1 downto 0);
  signal ctrl_rs1           : std_logic_vector(N_XLEN-1 downto 0);
  signal ctrl_rs1_valid     : std_logic;
  signal ctrl_rs2_valid     : std_logic;

  -- Hazard unit
  signal rs1_forwad_check : std_logic;
  signal rs2_forwad_check : std_logic;
  signal bubble           : std_logic;

  -- Register file output
  signal data_rs1 : std_logic_vector(N_DATA-1 downto 0);
  signal data_rs2 : std_logic_vector(N_DATA-1 downto 0);

begin

  -- ***********************************
  -- Hazard Unit: detect data hazard due to loads, example:
  -- lw  x1,DATA
  -- add x3,x2,x1  -> add need the new x1 value, but can't get it through
  --                  the current forwarding, so we stall the pipeline for 1 clock period
  rs1_forwad_check <= '1' when (ex2id_i.rd = ctrl_rs1)  and (ctrl_rs1_valid = '1') else '0';
  rs2_forwad_check <= '1' when (ex2id_i.rd = instr_rs2) and (ctrl_rs2_valid = '1') else '0';

  bubble   <= not(flush_i) and ex2id_i.mread and (rs1_forwad_check or rs2_forwad_check);
  bubble_o <= bubble;

    -- ***********************************
  -- Pipeline Register
  pipe_ex_o <= r_pipe_ex;
  process (clk_i)
  begin
    if (rising_edge(clk_i)) then
      if (rst_i = '1') then
        r_pipe_ex <= rstRegisterEX;

      elsif (flush_i = '1') then
        r_pipe_ex <= rstRegisterEX;

      elsif (stall_i = '1') then
        null;

      elsif (bubble = '1') then
        r_pipe_ex <= rstRegisterEX;

      else
        r_pipe_ex.bht_state     <= pipe_id_i.bht_state;
        r_pipe_ex.valid         <= pipe_id_i.valid;
        r_pipe_ex.regfile_we    <= ctrl_regfile_write;
        r_pipe_ex.regfile_sel0  <= ctrl_regfile_sel0;
        r_pipe_ex.branch_unc    <= ctrl_branch_unc;
        r_pipe_ex.branch        <= ctrl_branch;
        r_pipe_ex.branch_sel    <= ctrl_branch_sel;
        r_pipe_ex.mread         <= ctrl_mread;
        r_pipe_ex.mwrite        <= ctrl_mwrite;
        r_pipe_ex.csr_en        <= ctrl_csr_en;
        r_pipe_ex.excp_instr    <= ctrl_excp_instr; -- always '0' here
        r_pipe_ex.pc_arg_sel    <= ctrl_pc_arg_sel;
        r_pipe_ex.alu_op        <= ctrl_alu_op;
        r_pipe_ex.alu_sela      <= ctrl_alu_sela;
        r_pipe_ex.alu_selb      <= ctrl_alu_selb;
        r_pipe_ex.pc            <= pipe_id_i.pc;
        r_pipe_ex.data_rs1      <= data_rs1;
        r_pipe_ex.data_rs2      <= data_rs2;
        r_pipe_ex.imm           <= ctrl_imm;
        r_pipe_ex.funct3        <= instr_funct3;
        r_pipe_ex.rs1           <= ctrl_rs1;
        r_pipe_ex.rs2           <= instr_rs2;
        r_pipe_ex.rd            <= instr_rd;

      end if;
    end if;
  end process;

  -- ***************
  i_register_file : entity work.register_file
  generic map(
    N_DATA => N_DATA,
    N_ADDR => N_XLEN
  )
  port map(
    clk_i       => clk_i,
    rst_i       => rst_i,
    rs1_i       => ctrl_rs1,
    rs2_i       => instr_rs2,
    rd_i        => wb2id_i.rd,
    data_rd_i   => wb2id_i.data_rd,
    data_rs1_o  => data_rs1,
    data_rs2_o  => data_rs2,
    we_i        => wb2id_i.regfile_we
  );

  -- ***************
  i_core_instr_decoder: entity work.core_instr_decoder
  port map(
    instr_i         => pipe_id_i.instr,
    instr_decoded_o => instr_decoded,
    regfile_write_o => ctrl_regfile_write,
    regfile_sel0_o  => ctrl_regfile_sel0,
    branch_unc_o    => ctrl_branch_unc,
    branch_o        => ctrl_branch,
    branch_sel_o    => ctrl_branch_sel,
    mread_o         => ctrl_mread,
    mwrite_o        => ctrl_mwrite,
    csr_en_o        => ctrl_csr_en,
    pc_arg_sel_o    => ctrl_pc_arg_sel,
    alu_op_o        => ctrl_alu_op,
    alu_sela_o      => ctrl_alu_sela,
    alu_selb_o      => ctrl_alu_selb,
    excp_instr_o    => ctrl_excp_instr,
    imm_o           => ctrl_imm,
    rs1_o           => ctrl_rs1,
    rs1_valid_o     => ctrl_rs1_valid,
    rs2_valid_o     => ctrl_rs2_valid
  );

end architecture behavior;
