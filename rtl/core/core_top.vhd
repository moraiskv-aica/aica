library ieee;
use ieee.std_logic_1164.all;

use work.core_pkg.all;

-- *****************************************************************************
entity core_top is
  generic(
    USE_PREDICTOR : boolean := false  -- enable branch predictor
  );
  port(
    clk_i           : in  std_logic;
    rst_i           : in  std_logic;
    irq_ext_i       : in  std_logic;                              -- external interrupt request
    irq_ack_o       : out std_logic;                              -- interrupt request aknowledge
    imem_wait_i     : in  std_logic;                              -- wait for instr.
    imem_req_o      : out std_logic;                              -- valid flag, icache address
    imem_addr_o     : out std_logic_vector(N_DATA-1 downto ILSB); -- instr. address
    imem_data_i     : in  std_logic_vector(N_DATA-1 downto 0);    -- instr. input
    dmem_wait_i     : in  std_logic;                              -- wait for data
    dmem_req_o      : out std_logic;                              -- valid flag, dcache address
    dmem_we_byte_o  : out std_logic_vector(3 downto 0);           -- data memory write byte enable
    dmem_addr_o     : out std_logic_vector(N_DATA-1 downto 0);    -- data address
    dmem_data_i     : in  std_logic_vector(N_DATA-1 downto 0);    -- data from data memory
    dmem_data_o     : out std_logic_vector(N_DATA-1 downto 0)     -- data to data memory
  );
end entity core_top;

-- *****************************************************************************
architecture behavior of core_top is

  -- Reset, Flush and Stall flags
  signal flush_stage2_id  : std_logic;
  signal flush_stage3_ex  : std_logic;
  signal flush_stage4_mem : std_logic;

  signal stall_stage1_if  : std_logic;
  signal stall_stage2_id  : std_logic;
  signal stall_stage3_ex  : std_logic;
  signal stall_stage4_mem : std_logic;

  signal flush_branch : std_logic;  -- flush from branch/jump
  signal bubble_id    : std_logic;  -- bubble flag from stage id

  -- Exceptions signals/flags
  signal flush_excp  : std_logic;        -- flush from exception/interrupt
  signal excp_daddr  : std_logic;        -- flush flag from a bad data address created in a load or store
  signal m_tvec_base : std_logic_vector(N_DATA-1 downto ILSB); -- trap handler address

  -- Pipeline Registers (signals only here)
  signal pipe_id : t_pipe_id;
  signal pipe_ex : t_pipe_ex;
  signal pipe_wb : t_pipe_wb;

  -- Data between stages
  signal wb2if : t_wb2if;
  signal ex2id : t_ex2id;
  signal wb2id : t_wb2id;

  -- Data from stages
  signal wb_pc_sel      : std_logic;
  signal wb_branch_addr : std_logic_vector(N_DATA-1 downto ILSB);
  signal wb_data_rd     : std_logic_vector(N_DATA-1 downto 0);

  --- Forward
  signal fw_sela : std_logic;
  signal fw_selb : std_logic;

begin

  -- ***********************************
  -- Generate control signals for each pipeline register
  -- Priority: Flush > Stall > Bubble

  -- flush_stage1_if  <= flush_branch or flush_excp;  --NOTE: inside stage1, where flush_branch_o is generated
  flush_stage2_id  <= flush_branch or flush_excp;
  flush_stage3_ex  <= flush_branch or flush_excp;
  flush_stage4_mem <= flush_excp; -- stage after branch assertion, thus, a flush is only to discard instructions that generated exceptions

  stall_stage1_if  <= dmem_wait_i or imem_wait_i or bubble_id;
  stall_stage2_id  <= dmem_wait_i;
  stall_stage3_ex  <= dmem_wait_i;
  stall_stage4_mem <= dmem_wait_i;

  -- exception caused by bad data address (created in a load or store)
  excp_daddr <= pipe_wb.excp_mem;
  flush_excp <= ((excp_daddr or pipe_wb.excp_instr) and pipe_wb.excp_en) or pipe_wb.irq;

  -- ***********************************
  -- data from MEM to IF
  wb2if.bht_state    <= pipe_wb.bht_state;
  wb2if.branch       <= pipe_wb.branch and (not(flush_excp));  -- prevents from updating the BHT on a exception trigger
  wb2if.pc_sel       <= wb_pc_sel;
  wb2if.branch_addr  <= wb_branch_addr;
  wb2if.pc           <= pipe_wb.pc;

  -- data from EX to ID
  ex2id.mread <= pipe_ex.mread;
  ex2id.rd    <= pipe_ex.rd;

  -- data from WB to ID
  wb2id.regfile_we <= pipe_wb.regfile_we;
  wb2id.data_rd    <= wb_data_rd;
  wb2id.rd         <= pipe_wb.rd;

  -- ***********************************
  -- Forwarding
  -- The closest stage has priority, since it's the newest change

  fw_sela <= '1' when ( (pipe_wb.regfile_we = '1') and (pipe_ex.rs1 = pipe_wb.rd) ) else '0';
  fw_selb <= '1' when ( (pipe_wb.regfile_we = '1') and (pipe_ex.rs2 = pipe_wb.rd) ) else '0';

  -- ***********************************
  -- Pipeline stages

  -- Instructio fetch
  i_core_stage1_if: entity work.core_stage1_if
  generic map(
    USE_PREDICTOR => USE_PREDICTOR
  )
  port map(
    rst_i          => rst_i,
    clk_i          => clk_i,
    stall_i        => stall_stage1_if,
    imem_req_o     => imem_req_o,
    flush_branch_o => flush_branch,
    flush_excp_i   => flush_excp,
    mtvec_base_i   => m_tvec_base,
    wb2if_i        => wb2if,
    pipe_id_o      => pipe_id,
    imem_data_i    => imem_data_i,
    imem_addr_o    => imem_addr_o
  );

  -- Instruction decode
  i_core_stage2_id: entity work.core_stage2_id
  port map(
    rst_i       => rst_i,
    clk_i       => clk_i,
    flush_i     => flush_stage2_id,
    stall_i     => stall_stage2_id,
    bubble_o    => bubble_id,
    ex2id_i     => ex2id,
    wb2id_i     => wb2id,
    pipe_id_i   => pipe_id,
    pipe_ex_o   => pipe_ex
  );

  -- Execute
  i_core_stage3_ex: entity work.core_stage3_ex
  port map(
    rst_i           => rst_i,
    clk_i           => clk_i,
    flush_i         => flush_stage3_ex,
    flush_excp_i    => flush_excp,
    stall_i         => stall_stage3_ex,
    irq_ext_i       => irq_ext_i,
    irq_ack_o       => irq_ack_o,
    excp_daddr_i    => excp_daddr,
    excp_instr_i    => pipe_wb.excp_instr,
    irq_i           => pipe_wb.irq,
    mepc_i          => pipe_wb.pc,
    mtvec_base_i    => m_tvec_base,
    fw_sela_i       => fw_sela,
    fw_selb_i       => fw_selb,
    fw_wb_data_i    => wb_data_rd,
    dmem_data_o     => dmem_data_o,
    dmem_addr_o     => dmem_addr_o,
    dmem_we_byte_o  => dmem_we_byte_o,
    dmem_req_o      => dmem_req_o,
    pipe_ex_i       => pipe_ex,
    pipe_wb_o       => pipe_wb
  );

  -- Memory
  i_core_stage4_wb: entity work.core_stage4_wb
  port map(
    rst_i         => rst_i,
    clk_i         => clk_i,
    flush_i       => flush_stage4_mem,
    stall_i       => stall_stage4_mem,
    dmem_data_i   => dmem_data_i,
    pc_sel_o      => wb_pc_sel,
    branch_addr_o => wb_branch_addr,
    data_rd_o     => wb_data_rd,
    pipe_wb_i     => pipe_wb
  );

end architecture behavior;
