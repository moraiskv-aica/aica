library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.core_pkg.all;
use work.riscv_pkg.all;
use work.alu_pkg.all;

-- *****************************************************************************
entity core_stage3_ex is
  port(
    rst_i           : in  std_logic;
    clk_i           : in  std_logic;
    -- pipeline control
    stall_i         : in  std_logic;                              -- insert nop instruction
    flush_i         : in  std_logic;
    flush_excp_i    : in  std_logic;                              -- flush from exception
    -- interrupt flag
    irq_ext_i       : in  std_logic;                              -- external enterrupt request
    irq_i           : in  std_logic;                              -- inner interrupt flag
    irq_ack_o       : out std_logic;                              -- interrupt request acknowledge
    -- csr / exceptions data
    excp_daddr_i    : in  std_logic;                              -- exception from invalid data address
    excp_instr_i    : in  std_logic;                              -- exception from invalid instruction
    mepc_i          : in  std_logic_vector(N_DATA-1 downto ILSB); -- csr register mepc
    mtvec_base_i    : out std_logic_vector(N_DATA-1 downto ILSB); -- csr register mtvec
    -- forward signals
    fw_selb_i       : in  std_logic;                              -- MUX B control signal
    fw_sela_i       : in  std_logic;                              -- MUX A control signal
    fw_wb_data_i    : in  std_logic_vector(N_DATA-1 downto 0);    -- data from stage WB
    -- data memory
    dmem_data_o     : out std_logic_vector(N_DATA-1 downto 0);    -- data to data memory
    dmem_addr_o     : out std_logic_vector(N_DATA-1 downto 0);    -- data memory address (store and load)
    dmem_we_byte_o  : out std_logic_vector(3 downto 0);           -- data memory write byte enable
    dmem_req_o      : out std_logic;                              -- data memory request access
    -- stage pipeline register, ID/EX and EX/WB
    pipe_ex_i       : in  t_pipe_ex;
    pipe_wb_o       : out t_pipe_wb
  );
end entity core_stage3_ex;

-- *****************************************************************************
architecture behavior of core_stage3_ex is

  signal r_pipe_wb : t_pipe_wb;

  signal dmem_addr  : std_logic_vector(N_DATA-1 downto 0);  -- data memory address
  signal dmem_req   : std_logic;

  signal pc_offset : std_logic_vector(N_DATA-1 downto 0); -- pc + the selected offset
  signal pc_imm    : std_logic_vector(N_DATA-1 downto 0); -- pc + imm

  -- ALU input and output
  signal alu_value_b  : std_logic_vector(N_DATA-1 downto 0);
  signal alu_result   : std_logic_vector(N_DATA-1 downto 0);
  signal alu_zero     : std_logic;

  -- ALU control signals
  signal funct3_decoded : std_logic_vector(2 downto 0);
  signal instr_funct7   : std_logic;    -- instr. bit [30], which is always imm[10] when relevant
  signal alu_opcode     : std_logic_vector(3 downto 0);

  signal mux_alua : std_logic_vector(N_DATA-1 downto 0); -- select alu operand A
  signal mux_alub : std_logic_vector(N_DATA-1 downto 0); -- select alu operand B
  signal mux_alt  : std_logic_vector(N_DATA-1 downto 0); -- select data between pc+imm and a CSR

  -- exception/interrupt registers and flags
  constant CSR_ZERO : std_logic_vector(N_DATA-1 downto 5) := (others => '0');

  signal csr_new_data           : std_logic_vector(N_DATA-1 downto 0);
  signal csr_data               : std_logic_vector(N_DATA-1 downto 0);
  signal csr_sel                : std_logic_vector(3 downto 0);
  signal excp_en                : std_logic;
  signal irq_en                 : std_logic;
  signal irq_valid              : std_logic;
  signal excp_mem_aligned       : std_logic;    -- exception from miss aligned access to full word
  signal excp_mem_half_aligned  : std_logic;    -- exception from miss aligned access to half word
  signal excp_mem               : std_logic;

  -- control signals created due to the MRET instruction
  signal mret       : std_logic;
  signal branch_unc : std_logic;

begin

  irq_valid <= irq_en and irq_ext_i and pipe_ex_i.valid;
  irq_ack_o <= irq_valid;

  -- CSR Logic
  -- create the ctrl signal MRET, and set the branch_unc flag (if mret = '1'), so we can use
  -- the same logic path to realize the jump to EPC, it doesnt check for SRET/URET
  mret <= '1' when (pipe_ex_i.funct3 = "000" and pipe_ex_i.imm(1 downto 0) = "10") else '0';

  branch_unc <= (mret and pipe_ex_i.csr_en) or pipe_ex_i.branch_unc;

  -- choose the data input between data_rs1 and rs1
  csr_new_data <=
    mux_alua when (pipe_ex_i.funct3(2) = '0') else
    CSR_ZERO & pipe_ex_i.rs1;

  -- select EPC if the SYS instr. is a MRET
  csr_sel <=
    CSR_MEPC_OP when (mret = '1') else
    pipe_ex_i.imm(6)&pipe_ex_i.imm(2 downto 0);

  excp_mem_aligned <=
    '1'  when (dmem_addr(1 downto 0) /= "00" and pipe_ex_i.funct3(1) = '1') else
    '0';

  excp_mem_half_aligned <=
    '1' when (dmem_addr(0) = '1' and pipe_ex_i.funct3(1 downto 0) = FUNCT3_LSH(1 downto 0)) else
    '0';

  excp_mem <=
    '1'  when ((excp_mem_aligned = '1' or excp_mem_half_aligned = '1') and (pipe_ex_i.mread = '1' or pipe_ex_i.mwrite = '1')) else
    '0';

  -- PC + immediate
  -- pc_arg_sel
  pc_imm    <= std_logic_vector(unsigned(pipe_ex_i.pc&"0") + unsigned(pipe_ex_i.imm));
  pc_offset <=
    pc_imm when (pipe_ex_i.pc_arg_sel = '0') else
    std_logic_vector(unsigned(pipe_ex_i.pc&"0") + 4);

  -- MUX excp do datapath
  mux_alt <=
    csr_data when (pipe_ex_i.csr_en = '1') else
    pc_offset;

  -- ALU MUX(s)
  -- MUX A, define opr_A
  mux_alua <=
    pipe_ex_i.pc&"0"   when (pipe_ex_i.alu_sela  = '1') else
    fw_wb_data_i       when (fw_sela_i = '1')           else
    pipe_ex_i.data_rs1; -- when (fw_sela_i = '0');

  -- MUX 2 and MUX B, define opr_B
  mux_alub <=
    fw_wb_data_i       when (fw_selb_i = '1') else
    pipe_ex_i.data_rs2; -- when (fw_selb_i = '0');

    -- MUX2
  alu_value_b <=
    mux_alub when (pipe_ex_i.alu_selb = '0') else
    pipe_ex_i.imm; -- when (pipe_ex_i.alu_selb = '1');

  -- ***********************************
  -- ALU control unit
  instr_funct7 <=
    '0' when (pipe_ex_i.alu_op = ENC_ALU_ADD or (pipe_ex_i.alu_op = ENC_ALU_MISI and pipe_ex_i.funct3(0) = '0')) else
    pipe_ex_i.imm(10);

  funct3_decoded <=
    FUNCT3_ADD  when (pipe_ex_i.alu_op = ENC_ALU_ADD)                                  else
    FUNCT3_SLT  when (pipe_ex_i.alu_op = ENC_ALU_BRANCH and pipe_ex_i.funct3(1) = '0') else
    FUNCT3_SLTU when (pipe_ex_i.alu_op = ENC_ALU_BRANCH and pipe_ex_i.funct3(1) = '1') else
    pipe_ex_i.funct3;

  with instr_funct7&funct3_decoded select
    alu_opcode <= 
      -- Arith Operations => "00--"
      ALU_ADD  when '0'&FUNCT3_ADD,   -- ADD
      ALU_SUB  when	'1'&FUNCT3_ADD,   -- SUB
      ALU_SLL  when '-'&FUNCT3_SLL,   -- SLL
      ALU_SLT  when '-'&FUNCT3_SLT,   -- STL
      -- Logic Operations => "01--"
      ALU_OR   when '-'&FUNCT3_OR,    -- or
      ALU_AND  when '-'&FUNCT3_AND,   -- and
      ALU_XOR  when '-'&FUNCT3_XOR,   -- xor
      ALU_SLTU when '-'&FUNCT3_SLTU,  -- STLU
      -- Shift Operations => "100-"
      ALU_SRL  when '0'&FUNCT3_SRL,   -- SRL
      ALU_SRA  when '1'&FUNCT3_SRL,   -- SRA
      -- Dont Care
      "----"   when others;

  -- ***********************************
  -- Store Unit
  -- dedicated adder for memory address
  dmem_addr_o <= dmem_addr;
  dmem_data_o <= mux_alub;
  dmem_req_o  <= dmem_req;

  dmem_addr <= std_logic_vector(unsigned(mux_alua) + unsigned(pipe_ex_i.imm));
  dmem_req  <= (pipe_ex_i.mwrite or pipe_ex_i.mread) and not(flush_i);

  -- Write enable with byte select
  dmem_we_byte_o <=
  "0000" when (dmem_req = '0') or (pipe_ex_i.mwrite = '0')              else
  "0001" when (pipe_ex_i.funct3(1) = '0' and pipe_ex_i.funct3(0) = '0') else  -- SB
  "0011" when (pipe_ex_i.funct3(1) = '0' and pipe_ex_i.funct3(0) = '1') else  -- SH
  "1111";                                                                     -- SW

  -- ***********************************
  -- Pipeline Register
  pipe_wb_o <= r_pipe_wb;
  process (clk_i)
  begin
    if (rising_edge(clk_i)) then
      if (rst_i = '1') then
        r_pipe_wb <= rstRegisterWB;

      elsif (flush_i = '1') then
        r_pipe_wb <= rstRegisterWB;

      elsif (stall_i = '1') then
        -- stall will occur due to the memory latency, we cannot insert
        -- a bubble here since that would discard the forward data
        null;

      else
        -- control signals
        r_pipe_wb.bht_state    <= pipe_ex_i.bht_state;
        r_pipe_wb.regfile_we   <= pipe_ex_i.regfile_we;
        r_pipe_wb.regfile_sel0 <= pipe_ex_i.regfile_sel0;
        r_pipe_wb.branch_unc   <= branch_unc;
        r_pipe_wb.branch       <= pipe_ex_i.branch;
        r_pipe_wb.branch_sel   <= pipe_ex_i.branch_sel;
        r_pipe_wb.mread        <= pipe_ex_i.mread;
        r_pipe_wb.mwrite       <= pipe_ex_i.mwrite;
        r_pipe_wb.excp_en      <= excp_en;
        r_pipe_wb.irq          <= irq_valid;
        r_pipe_wb.excp_mem     <= excp_mem;
        r_pipe_wb.excp_instr   <= pipe_ex_i.excp_instr;
        -- others
        r_pipe_wb.alt_result   <= mux_alt;
        r_pipe_wb.pc           <= pipe_ex_i.pc;
        r_pipe_wb.alu_result   <= alu_result;
        r_pipe_wb.alu_zero     <= alu_zero;
        r_pipe_wb.funct3       <= pipe_ex_i.funct3;
        r_pipe_wb.rs2          <= pipe_ex_i.rs2;
        r_pipe_wb.rd           <= pipe_ex_i.rd;

      end if;
    end if;
  end process;

  -- ***********************************
  i_core_csr: entity work.core_csr
  port map(
    rst_i         => rst_i,
    clk_i         => clk_i,
    flush_i       => flush_i,
    flush_excp_i  => flush_excp_i,
    excp_daddr_i  => excp_daddr_i,
    excp_instr_i  => excp_instr_i,
    irq_i         => irq_i,
    csr_en_i      => pipe_ex_i.csr_en,
    csr_op_i      => pipe_ex_i.funct3,
    csr_sel_i     => csr_sel,
    mepc_i        => mepc_i,
    mtvec_base_o  => mtvec_base_i,
    irq_en_o      => irq_en,
    excp_en_o     => excp_en,
    csr_data_i    => csr_new_data,
    csr_data_o    => csr_data
  );

  i_alu : entity work.alu
  generic map(
    N =>  N_DATA
  )
  port map(
    value_a_i  => mux_alua,
    value_b_i  => alu_value_b,
    opcode_i   => alu_opcode,
    result_o   => alu_result,
    zero_res_o => alu_zero
  );

end architecture behavior;
