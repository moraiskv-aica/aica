library ieee;
use ieee.std_logic_1164.all;

use work.core_pkg.all;
use work.riscv_pkg.all;

-- *****************************************************************************
-- Decode the instruction opcode (6:2) for the control signals
-- through the datapath, and also detects invalid instructions, checking the
-- instr. 2 lsb.
entity core_instr_decoder is
  port(
    instr_i         : in  std_logic_vector(N_DATA-1 downto 0);  -- full instruction
    instr_decoded_o : out std_logic_vector(N_DATA-1 downto 0);  -- instruction decoded (after RV extender)
    regfile_write_o : out std_logic;                            -- enable reg file write
    regfile_sel0_o  : out std_logic;                            -- reg file input data selector
    branch_unc_o    : out std_logic;                            -- unconditional branch (jumps)
    branch_o        : out std_logic;                            -- conditional branch
    branch_sel_o    : out std_logic;                            -- branch address selector
    mread_o         : out std_logic;                            -- memory read access
    mwrite_o        : out std_logic;                            -- memory write access
    csr_en_o        : out std_logic;                            -- control/status register enable
    pc_arg_sel_o    : out std_logic;                            -- pc adder arg at stage ex
    alu_op_o        : out std_logic_vector(1 downto 0);         -- alu opcode
    alu_sela_o      : out std_logic;                            -- alu value a selector
    alu_selb_o      : out std_logic;                            -- alu value b selector
    excp_instr_o    : out std_logic;                            -- exception from invalid instruction
    imm_o           : out std_logic_vector(N_DATA-1 downto 0);  -- immediate field already extended
    rs1_o           : out std_logic_vector(N_XLEN-1 downto 0);  -- register file source 1
    rs1_valid_o     : out std_logic;                            -- true if the rs1 field is valid (i.e., not some random immediate value)
    rs2_valid_o     : out std_logic                             -- true if the rs2 field is valid (i.e., not some random immediate value)
  );
end entity core_instr_decoder;

-- *****************************************************************************
architecture behavior of core_instr_decoder is

  signal instr_decoded : std_logic_vector(N_DATA-1 downto 0);
  signal instr_rvc_ext : std_logic_vector(N_DATA-1 downto 0);

  alias instr_rvc    : std_logic_vector(15 downto 0) is instr_i(15 downto 0);

  alias instr_imm    : std_logic_vector(N_DATA-1 downto 7) is instr_decoded(N_DATA-1 downto 7); -- instruction immediate field (not decoded)
  alias instr_rs1    : std_logic_vector(N_XLEN-1 downto 0) is instr_decoded(19 downto 15);      -- instruction register file source 1
  alias instr_rd     : std_logic_vector(N_XLEN-1 downto 0) is instr_decoded(11 downto 7);       -- instruction destiny register
  alias instr_opcode : std_logic_vector(6 downto 0)        is instr_decoded(6 downto 0);        -- instruction opcode

  signal rd_zero : std_logic; --true if rd is set to x0

  -- instruction type
  signal instr_name : string (1 to 6);

  -- control signals to be disabled if detected a invalid instruction
  signal regfile_write  : std_logic;
  signal branch_unc     : std_logic;
  signal branch         : std_logic;
  signal mread          : std_logic;
  signal mwrite         : std_logic;
  signal csr_en         : std_logic;
  signal valid_instr    : std_logic;

begin

  rd_zero <= '0' when (instr_rd /= X0_ADDR) else '1';

  -- Invalid Instruction Logic
  valid_instr  <= '1' when (instr_opcode(1 downto 0) = RV32_VALID_BITS) else '0';
  excp_instr_o <= not(valid_instr);

  -- ***********************************
  -- Stage WB
  -- ***********************************
  with instr_opcode(5 downto 2) select
    regfile_write <=
      '0' when "1000",  -- OPCODE_STORE[3:0] = OPCODE_BRANCH[3:0] = "1000"
      '1' when others;

  regfile_write_o <= '1' when (regfile_write = '1') and (valid_instr = '1') and (rd_zero = '0') else '0';

  with instr_opcode(6 downto 2) select
    regfile_sel0_o <=
      '0' when OPCODE_JALR | OPCODE_JAL | OPCODE_SYSTEM,
      '1' when others;

  with instr_opcode(6 downto 2) select
    branch_unc <=
      '1' when OPCODE_JAL | OPCODE_JALR,
      '0' when others;

  branch_unc_o <= branch_unc and valid_instr;

  with instr_opcode(6 downto 2) select
    branch <=
      '1' when OPCODE_BRANCH,
      '0' when others;

  branch_o <= branch and valid_instr;

  with instr_opcode(6 downto 2) select
    branch_sel_o <=
      '1' when OPCODE_JALR | OPCODE_JAL,
      '0' when others;

  with instr_opcode(6 downto 2) select
    mread <=
      '1' when OPCODE_LOAD,   -- funct3 define LW/LH/LB, in WB stage
      '0' when others;

  mread_o <= '1' when (mread = '1') and (valid_instr = '1') and (rd_zero = '0') else '0'; -- disable if rd=0, we want to avoid unnecessary stalls

  with instr_opcode(6 downto 2) select
    mwrite <=
      '1' when OPCODE_STORE,  -- funct3 define SW/SH/SB, in WB stage
      '0' when others;

  mwrite_o <= mwrite and valid_instr;

  -- ***********************************
  -- Stage EX
  -- ***********************************
  with instr_opcode(6 downto 2) select
    csr_en <=
      '1' when OPCODE_SYSTEM, -- funct3 define the atomic operation above the CSRegister
      '0' when others;

  csr_en_o <= csr_en and valid_instr;

  with instr_opcode(6 downto 2) select
    pc_arg_sel_o <=
      '1' when OPCODE_JAL | OPCODE_JALR,  -- pc+4
      '0' when others;                    -- pc+immediate

  with instr_opcode(6 downto 2) select
    alu_op_o <=
      ENC_ALU_MISR   when OPCODE_OPREG,   -- ALU-funct3 => reg to reg op
      ENC_ALU_MISI   when OPCODE_OPIMM,   -- ALU-funct3 => reg to imm op
      ENC_ALU_BRANCH when OPCODE_BRANCH,  -- branch-funct3
      ENC_ALU_ADD    when others;         -- ADD

  with instr_opcode(6 downto 2) select
    alu_sela_o <=
      '1' when OPCODE_AUIPC | OPCODE_JAL, -- pc
      '0' when others;                    -- data rs1

  with instr_opcode(6 downto 2) select
    alu_selb_o <=
      '1' when OPCODE_AUIPC | OPCODE_LUI | OPCODE_JAL | OPCODE_JALR | OPCODE_OPIMM,
      '0' when others;    -- data rs2

  -- ***********************************
  -- Stage ID
  -- ***********************************
  rs1_o <=
    instr_rs1 when (instr_opcode(6 downto 2) /= OPCODE_LUI) else
    (others => '0');

  with instr_opcode(6 downto 2) select
    rs1_valid_o <=
      '0' when OPCODE_LUI | OPCODE_AUIPC | OPCODE_JAL | OPCODE_JALR,
      '1' when others;

  with instr_opcode(6 downto 2) select
    rs2_valid_o <=
      '1' when OPCODE_BRANCH | OPCODE_STORE | OPCODE_OPREG,
      '0' when others;

  -- RVC decoder (extend to it's 32 bits equivalent)
  instr_decoded_o <= instr_decoded;
  instr_decoded   <=
    instr_i when (instr_i(1 downto 0) = "11") else
    instr_rvc_ext;

  instr_rvc_ext <= (others => '0'); --TODO: implement decoder/extender

  -- ***********************************
  -- Immediate Decoder
  -- ***********************************
  i_core_immediate: entity work.core_immediate
  port map(
    opcode_i    => instr_opcode(6 downto 2),
    imm_field_i => instr_imm,
    imm_o       => imm_o
  );

  -- ***********************************
  -- String (SIM only)
  -- ***********************************
  with instr_opcode(6 downto 2) select
    instr_name <=
      "SYSTEM" when OPCODE_SYSTEM,
      "LUI___" when OPCODE_LUI,
      "AUIPC_" when OPCODE_AUIPC,
      "JAL___" when OPCODE_JAL,
      "JALR__" when OPCODE_JALR,
      "BRANCH" when OPCODE_BRANCH,
      "LOAD__" when OPCODE_LOAD,
      "STORE_" when OPCODE_STORE,
      "REGIMM" when OPCODE_OPIMM,
      "REGREG" when OPCODE_OPREG,
      "___INV" when others;

end architecture behavior;
