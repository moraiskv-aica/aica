library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.alu_pkg.all;

-- *****************************************************************************
entity alu is
  generic(
    N : integer := 32  -- data size
  );
  port(
    value_a_i   : in  std_logic_vector(N-1 downto 0);
    value_b_i   : in  std_logic_vector(N-1 downto 0);
    opcode_i    : in  std_logic_vector(3 downto 0);     -- alu operation control
    result_o    : out std_logic_vector(N-1 downto 0);
    zero_res_o  : out std_logic                         -- 0 when 'value_a_i - value_b_i = 0'
  );
end entity alu;

-- *****************************************************************************
architecture behavior of alu is

  -- constant 0 for comparison
  constant ZERO_DATA : std_logic_vector(N-1 downto 0) := (others => '0');

  signal vala_less : std_logic;             -- '1' when value_a_i < value_b_i (unsigned)
  signal shamt     : integer range 0 to 31; -- shift amount

  -- operation
  signal result_add  : std_logic_vector(N-1 downto 0);
  alias  result_sub  : std_logic_vector(N-1 downto 0) is result_add;
  signal result_sll  : std_logic_vector(N-1 downto 0);
  signal result_slt  : std_logic_vector(N-1 downto 0);
  signal result_or   : std_logic_vector(N-1 downto 0);
  signal result_and  : std_logic_vector(N-1 downto 0);
  signal result_xor  : std_logic_vector(N-1 downto 0);
  signal result_sltu : std_logic_vector(N-1 downto 0);
  signal result_srl  : std_logic_vector(N-1 downto 0);
  signal result_sra  : std_logic_vector(N-1 downto 0);

begin

  shamt <= to_integer(unsigned(value_b_i(4 downto 0)));

  -- unsigned compare, used with 'Set Less Than Unsigned'
  vala_less <= '1' when (value_a_i < value_b_i) else '0';

  -- ***********************************
  -- arith operations
  with opcode_i(0) select
    result_add <=
      std_logic_vector(unsigned(value_a_i) + unsigned(value_b_i)) when '0',   -- result_add
      std_logic_vector(unsigned(value_a_i) - unsigned(value_b_i)) when others;-- result_sub

  result_sll <= to_stdlogicvector(to_bitvector(value_a_i) sll shamt);
  result_slt <= (0 => result_sub(N-1), others => '0'); -- result_sub is select with opcode_i(0) = 1

  -- ***********************************
  -- logic operations
  result_or   <= value_a_i or  value_b_i;
  result_and  <= value_a_i and value_b_i;
  result_xor  <= value_a_i xor value_b_i;
  result_sltu <= (0 => vala_less, others => '0');

  -- ***********************************
  -- shift right operations
  result_srl <= to_stdlogicvector(to_bitvector(value_a_i) srl shamt);
  result_sra <= to_stdlogicvector(to_bitvector(value_a_i) sra shamt);

  -- ***************
  -- result logic
  with opcode_i select
    result_o <=
      result_add      when "000-",  --add/sub
      result_sll      when ALU_SLL,
      result_slt      when ALU_SLT,
      result_or       when ALU_OR,
      result_and      when ALU_AND,
      result_xor      when ALU_XOR,
      result_sltu     when ALU_SLTU,
      result_srl      when ALU_SRL,
      result_sra      when ALU_SRA,
      (others => '-') when others;

  -- compare the subtraction to 0, this is the actual result for branch operations
  zero_res_o <= '1' when (result_sub = ZERO_DATA) else '0';

end architecture behavior;
