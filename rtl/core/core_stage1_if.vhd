library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.core_pkg.all;
use work.riscv_pkg.all;

-- *****************************************************************************
entity core_stage1_if is
  generic(
    USE_PREDICTOR : boolean := false  -- enable branch predictor
  );
  port(
    rst_i           : in  std_logic;
    clk_i           : in  std_logic;
    -- pipeline control
    stall_i         : in  std_logic;
    flush_excp_i    : in  std_logic;
    flush_branch_o  : out std_logic;
    -- memory interface
    imem_req_o      : out std_logic;      -- imem request access
    -- stage stall/flush flag, and also exceptions data
    mtvec_base_i    : in  std_logic_vector(N_DATA-1 downto ILSB);
    -- signals from stage WB
    wb2if_i         : in  t_wb2if;
    -- stage pipeline register, and current pc/instr values
    pipe_id_o       : out t_pipe_id;	-- pipeline register
    imem_data_i     : in  std_logic_vector(N_DATA-1 downto 0);
    imem_addr_o     : out std_logic_vector(N_DATA-1 downto ILSB)
  );
end entity core_stage1_if;

-- *****************************************************************************
architecture behavior of core_stage1_if is

  -- FSM used to control the instruction fetch
  type t_fsm_ifetch is (
    FSM_IFETCH_DIS,     -- instruction fetch from memory is disabled, thus, imem_req_o and pc_we should also be
    FSM_IFETCH_START,   -- instruction fetch is enabled, but we are inserting bubble at the pipeline (since it's empty)
    FSM_IFETCH_READY);  -- instruction fetch is enabled fully enabled (returns to FSM_IFETCH_START on flush)

  signal r_ifetch_fsm : t_fsm_ifetch;
  signal r_pipe_id    : t_pipe_id;

  -- MUX 0: define the PC input
  -- MUX P: define the instr. fetch address
  -- MUX PC: define the PC input as the CSR MTVEC output in case of a exception/interrupt
  signal mux_iaddr  : std_logic_vector(N_DATA-1 downto ILSB);
  signal mux_pc     : std_logic_vector(N_DATA-1 downto ILSB);

  -- Program Counter
  signal r_pc      : std_logic_vector(N_DATA-1 downto ILSB);  -- PC register
  signal r_pc_temp : std_logic_vector(N_DATA-1 downto ILSB);  -- PC temporary register (save the instr fetch address from the previous clk cycle)
  signal pc_offset : std_logic_vector(N_DATA-1 downto ILSB);  -- PC + 4 or 2
  signal pc_we     : std_logic;                               -- PC write enable

  -- Predict Unit
  signal bht_miss     : std_logic_vector(1 downto 0);           -- miss state from the BHT check: "00" = ok; "10" = miss not taken ; "11" = miss taken; others don't care
  signal bht_taken    : std_logic;                              -- taken flag read from BHT for the current branch instruction
  signal bht_state    : std_logic_vector(2 downto 0);           -- fsm state read from BHT for the current branch instruction
  signal bht_target   : std_logic_vector(N_DATA-1 downto ILSB); -- target address read from BHT for the current branch instruction

  signal taken        : std_logic;                              -- '1' means a taken action from the branch predict unit

  -- flush and stall flags
  signal flush : std_logic;

begin

  -- stage stall/flush flags
  flush_branch_o <= bht_miss(1);
  flush          <= bht_miss(1) or flush_excp_i;

  -- predict taken flag
  taken <= bht_taken when (r_ifetch_fsm = FSM_IFETCH_READY) else '0';

  -- request instruction, disabled when the core is at rst state, or
  -- the current address is invalid due to a flush incoming
  imem_req_o <= '0' when (flush = '1') or (r_ifetch_fsm = FSM_IFETCH_DIS) else '1';

  -- instr. fetch address
  mux_iaddr <=
    bht_target when (taken = '1') else
    r_pc;

  imem_addr_o <= mux_iaddr;

  -- ***********************************
  -- Program Counter
  -- Program Counter write enable
  pc_we     <= '0' when (stall_i = '1' and flush = '0') or (r_ifetch_fsm = FSM_IFETCH_DIS) else '1';
  pc_offset <= std_logic_vector(unsigned(mux_iaddr) + 2);  -- +4 (lsb ignored)

  -- MUX PC : choose the PC input between PC+4, branch target from WB stage, or
  -- PC+4 from stage WB (needed if the predict unit miss)
  -- Jumps are always predicted as not_taken, triggering a miss
  mux_pc <=
    mtvec_base_i         when (flush_excp_i = '1')                      else
    -- pc from branch +4   when (bht_miss(1) = '1' and bht_miss(0) = '1') else
    wb2if_i.branch_addr when (bht_miss(1) = '1' and bht_miss(0) = '0') else
    pc_offset;

  -- pc register
  process(clk_i)
  begin
    if (rising_edge(clk_i)) then
      if (rst_i = '1') then
        r_pc <= (others => '0');
      elsif (pc_we = '1') then
        r_pc <= mux_pc;
      end if;
    end if;
  end process;

  -- pc temporary register, since it's expected a 1 cycle latency from the memory
  -- Program Counter Temporary Register
  process(clk_i)
  begin
    if (rising_edge(clk_i)) then
      if (rst_i = '1') then
        r_pc_temp <= (others => '0');
      elsif (stall_i = '0') then
        r_pc_temp <= mux_iaddr;
      end if;
    end if;
  end process;

  -- ***********************************
  -- Pipeline Register
  pipe_id_o <= r_pipe_id;
  process(clk_i)
  begin
    if (rising_edge(clk_i)) then
      if (rst_i = '1') then
        r_pipe_id <= rstRegisterID;

      elsif (flush = '1' or r_ifetch_fsm /= FSM_IFETCH_READY) then
        r_pipe_id <= rstRegisterID;

      elsif (stall_i = '1') then
        null;

      else
        r_pipe_id.bht_state <= bht_state;
        r_pipe_id.valid     <= '1';
        r_pipe_id.pc        <= r_pc_temp;
        r_pipe_id.instr     <= imem_data_i;

      end if;
    end if;
  end process;

  -- ***********************************
  -- FSM used to control the instruction fetch
  process(clk_i)
  begin
    if (rising_edge(clk_i)) then
      if (rst_i = '1') then
        r_ifetch_fsm <= FSM_IFETCH_DIS;
      else
        -- keep at rst state for 1 cycle, meanwhile some signals like disabled
        case r_ifetch_fsm is
          when FSM_IFETCH_DIS   => r_ifetch_fsm <= FSM_IFETCH_START;
          when FSM_IFETCH_START => r_ifetch_fsm <= FSM_IFETCH_READY;
          when FSM_IFETCH_READY =>
            if (flush) then
              r_ifetch_fsm <= FSM_IFETCH_START;
            end if;
        end case;
      end if;
    end if;
  end process;

  -- ***********************************
  -- Generate Branch History Table (BHT), or
  -- connect it's signals as always not taken (if disabled)
  gen_branch_predictor: if USE_PREDICTOR generate

    -- TODO: readd branch_predictor here
    bht_miss    <= "00";
    bht_taken   <= '0';
    bht_state   <= "000";
    bht_target  <= (others => '0');

  else generate

    -- branchs are always predicted as not taken without
    -- predictor, thus, a miss can only be about it
    bht_miss <=
      "10" when (wb2if_i.pc_sel = '1') else
      "00";

    bht_taken   <= '0';
    bht_state   <= "000";
    bht_target  <= (others => '0');

 end generate gen_branch_predictor;

end architecture behavior;
