library ieee;
use ieee.std_logic_1164.all;

-- *****************************************************************************
package riscv_pkg is

  constant XLEN     : integer := 32;    -- register file depth
  constant N_XLEN   : integer := 5;     -- register file address size => 2^(N_XLEN) = XLEN
  constant X0_ADDR  : std_logic_vector(N_XLEN-1 downto 0) := (others => '0');

  -- ***************
  -- instruction valid bits => [1:0]
  constant RV32_VALID_BITS : std_logic_vector(1 downto 0) := "11";

  -- instruction opcode => [6:2]
  constant OPCODE_LUI     : std_logic_vector(6 downto 2) := "01101";
  constant OPCODE_AUIPC   : std_logic_vector(6 downto 2) := "00101";
  constant OPCODE_JAL     : std_logic_vector(6 downto 2) := "11011";
  constant OPCODE_JALR    : std_logic_vector(6 downto 2) := "11001";
  constant OPCODE_BRANCH  : std_logic_vector(6 downto 2) := "11000";
  constant OPCODE_LOAD    : std_logic_vector(6 downto 2) := "00000";
  constant OPCODE_STORE   : std_logic_vector(6 downto 2) := "01000";
  constant OPCODE_OPIMM   : std_logic_vector(6 downto 2) := "00100";
  constant OPCODE_OPREG   : std_logic_vector(6 downto 2) := "01100";
  constant OPCODE_SYSTEM  : std_logic_vector(6 downto 2) := "11100";
  constant OPCODE_CSR     : std_logic_vector(6 downto 2) := "11100";  --same as SYSTEM

  -- funct3 opcode => [14:12]
  -- Logic/Arithmetic
  -- NOTE: some operations need another field for decoding,
  --       like ADD/SUB that are both encoded as "000"
  constant FUNCT3_ADD  : std_logic_vector(2 downto 0) := "000";
  constant FUNCT3_SLL  : std_logic_vector(2 downto 0) := "001";
  constant FUNCT3_SLT  : std_logic_vector(2 downto 0) := "010";
  constant FUNCT3_SLTU : std_logic_vector(2 downto 0) := "011";
  constant FUNCT3_XOR  : std_logic_vector(2 downto 0) := "100";
  constant FUNCT3_SRL  : std_logic_vector(2 downto 0) := "101";
  constant FUNCT3_OR   : std_logic_vector(2 downto 0) := "110";
  constant FUNCT3_AND  : std_logic_vector(2 downto 0) := "111";

  -- Load/Store
  constant FUNCT3_LSB  : std_logic_vector(2 downto 0) := "000"; -- load/store byte
  constant FUNCT3_LSH  : std_logic_vector(2 downto 0) := "001"; -- load/store half word
  constant FUNCT3_LSW  : std_logic_vector(2 downto 0) := "010"; -- load/store word
  constant FUNCT3_LBU  : std_logic_vector(2 downto 0) := "100"; -- load byte unsigned
  constant FUNCT3_LHU  : std_logic_vector(2 downto 0) := "101"; -- load half word unsigned

  -- ***************
  -- 12 lsb from the NOP instruction
  constant RV32I_NOP : std_logic_vector(31 downto 0) := x"0000_0013";

  -- ***************
  -- CSR
  constant CSR_TVEC_VECTOR_MODE : std_logic_vector(1 downto 0) := "10";

end package riscv_pkg;
