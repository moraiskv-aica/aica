library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- *****************************************************************************
-- (2^N_ADDR)xN_DATA = Register File:
-- 2 read ports, with data outputs of 32 bits;
-- 1 write port, with input data of 32 bits;
-- forward logic for both read ports, if a read is at the same
-- address being write, the new data is selected to the output;
-- register x0 (address 0) is the constant 0.
-- *****************************************************************************
entity register_file is
  generic(
    N_DATA : integer := 32; -- data size
    N_ADDR : integer := 5   -- address size
  );
  port(
    clk_i       : in  std_logic;
    rst_i       : in  std_logic;
    rs1_i       : in  std_logic_vector(N_ADDR-1 downto 0);-- rs1_i address input
    rs2_i       : in  std_logic_vector(N_ADDR-1 downto 0);-- rs2_i address input
    rd_i        : in  std_logic_vector(N_ADDR-1 downto 0);-- rd_i address input
    data_rd_i	  : in  std_logic_vector(N_DATA-1 downto 0);-- rd_i data input
    data_rs1_o  : out std_logic_vector(N_DATA-1 downto 0);-- rs1_i data output
    data_rs2_o  : out std_logic_vector(N_DATA-1 downto 0);-- rs2_i data output
    we_i        : in  std_logic                           -- write enable
  );
end entity register_file;

-- *****************************************************************************
architecture behavior of register_file is

  -- type array, 32x4bytes, register file
  type   t_registers is ARRAY(1 to (2**N_ADDR)-1) of std_logic_vector(N_DATA-1 downto 0);
  signal r_registers : t_registers;

  signal rs1_index : integer range 0 to (2**N_ADDR)-1;
  signal rs2_index : integer range 0 to (2**N_ADDR)-1;
  signal rd_index  : integer range 0 to (2**N_ADDR)-1;

begin

  -- input address as integer (to use as index)
  rs1_index <= (to_integer(unsigned(rs1_i)));
  rs2_index <= (to_integer(unsigned(rs2_i)));
  rd_index  <= (to_integer(unsigned(rd_i)));

  data_rs1_o <=
    (others => '0') when (rs1_index = 0)                         else -- 0 if register is at rst or rs1=x0
    data_rd_i       when (we_i = '1' and (rs1_index = rd_index)) else -- forward
    r_registers(rs1_index);

  data_rs2_o <=
    (others => '0') when (rs2_index = 0)                         else -- 0 if register is at rst or rs2=x0
    data_rd_i       when (we_i = '1' and (rs2_index = rd_index)) else -- forward
    r_registers(rs2_index);

  -- ***************
  proc_register_file_write : process (clk_i)
  begin
    if (rising_edge(clk_i)) then
      if (rst_i = '1') then
        r_registers <= (others => (others => '0'));
      elsif (we_i = '1') then
        r_registers(rd_index) <= data_rd_i;
      end if;
    end if;
  end process proc_register_file_write;

end architecture behavior;
