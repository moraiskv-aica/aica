// *****************************************************************************
// Author:      Kevin Paula Morais
// Date:        07/11/2024
// File:        vf_top.sv
// Description: Top module for verification.
// *****************************************************************************

`include "vf_utils.svh"

// *****************************************************************************
module vf_top ();

  localparam int N_DATA     = 32;
  localparam int N_RAM_ADDR = 26;
  localparam int ILSB       = 1;

  logic clk;
  logic rst;

  // Aica & RAM connections
  t_dut_ports dut_ports;
  t_dut_ports ref_ports;

  logic [N_RAM_ADDR-1:0] ram_addra;
  logic [N_RAM_ADDR-1:0] ram_addrb;

  logic [N_RAM_ADDR-1:0] ref_ram_addra;
  logic [N_RAM_ADDR-1:0] ref_ram_addrb;

  // *******************************************************
  // AICA Processor Instance
  assign dut_ports.clk = clk;
  assign dut_ports.rst = rst;
  assign dut_ports.irq = 1'b0;

  // Address Mapping
  assign ram_addra = {dut_ports.imem_addr[N_RAM_ADDR-1:ILSB], 1'b0};
  assign ram_addrb = dut_ports.dmem_addr[N_RAM_ADDR-1:0];

  aica_top #(
    .USE_PREDICTOR(0)
  ) i_dut (
    .clk_i          (dut_ports.clk),
    .rst_i          (dut_ports.rst),
    .irq_i          (dut_ports.irq),
    .irq_ack_o      (dut_ports.irq_ack),
    .imem_wait_i    (dut_ports.imem_wait),
    .imem_req_o     (dut_ports.imem_req),
    .imem_addr_o    (dut_ports.imem_addr),
    .imem_data_i    (dut_ports.imem_data),
    .dmem_wait_i    (dut_ports.dmem_wait),
    .dmem_req_o     (dut_ports.dmem_req),
    .dmem_we_byte_o (dut_ports.dmem_we_byte),
    .dmem_addr_o    (dut_ports.dmem_addr),
    .dmem_data_i    (dut_ports.dmem_ram_data),
    .dmem_data_o    (dut_ports.dmem_core_data)
  );

  // External RAM Instance
  ram_dp_tb #(
    .N_DATA   (N_DATA),
    .N_ADDR   (N_RAM_ADDR),
    .MEM_FILE ("test.mem")
  ) i_ram_dp_tb (
    .clk_i     (dut_ports.clk),
    .rst_i     (dut_ports.rst),
    .waita_o   (dut_ports.imem_wait),
    .ena_i     (dut_ports.imem_req),
    .addra_i   (ram_addra),
    .douta_o   (dut_ports.imem_data),
    .waitb_o   (dut_ports.dmem_wait),
    .enb_i     (dut_ports.dmem_req),
    .we_byte_i (dut_ports.dmem_we_byte),
    .addrb_i   (ram_addrb),
    .dinb_i    (dut_ports.dmem_core_data),
    .doutb_o   (dut_ports.dmem_ram_data)
  );

  // *******************************************************
  // AICA Reference Processor Instance
  assign ref_ports.clk = clk;
  assign ref_ports.rst = rst;
  assign ref_ports.irq = 1'b0;

  // Address Mapping
  assign ref_ram_addra = {ref_ports.imem_addr[N_RAM_ADDR-1:ILSB], 1'b0};
  assign ref_ram_addrb = ref_ports.dmem_addr[N_RAM_ADDR-1:0];

  vf_core_ref
  i_vf_core_ref (
    .clk_i          (ref_ports.clk),
    .rst_i          (ref_ports.rst),
    .irq_i          (ref_ports.irq),
    .irq_ack_o      (ref_ports.irq_ack),
    .imem_wait_i    (ref_ports.imem_wait),
    .imem_req_o     (ref_ports.imem_req),
    .imem_addr_o    (ref_ports.imem_addr),
    .imem_data_i    (ref_ports.imem_data),
    .dmem_wait_i    (ref_ports.dmem_wait),
    .dmem_req_o     (ref_ports.dmem_req),
    .dmem_we_byte_o (ref_ports.dmem_we_byte),
    .dmem_addr_o    (ref_ports.dmem_addr),
    .dmem_data_i    (ref_ports.dmem_ram_data),
    .dmem_data_o    (ref_ports.dmem_core_data)
  );

  // External RAM Instance
  ram_dp_tb #(
    .N_DATA   (N_DATA),
    .N_ADDR   (N_RAM_ADDR),
    .MEM_FILE ("test.mem")
  ) i_ref_ram_dp_tb (
    .clk_i     (ref_ports.clk),
    .rst_i     (ref_ports.rst),
    .waita_o   (ref_ports.imem_wait),
    .ena_i     (ref_ports.imem_req),
    .addra_i   (ref_ram_addra),
    .douta_o   (ref_ports.imem_data),
    .waitb_o   (ref_ports.dmem_wait),
    .enb_i     (ref_ports.dmem_req),
    .we_byte_i (ref_ports.dmem_we_byte),
    .addrb_i   (ref_ram_addrb),
    .dinb_i    (ref_ports.dmem_core_data),
    .doutb_o   (ref_ports.dmem_ram_data)
  );

  // *******************************************************
  // Verification modules
  vf_monitor
  i_vf_monitor (
    .clk_o        (clk),
    .rst_o        (rst),
    .dut_ports_i  (dut_ports),
    .ref_ports_i  (ref_ports)
  );

endmodule
