// *****************************************************************************
//      Author: Kevin Paula Morais
//        Date: 20/10/2023
//        File: vf_utils.svh
// Description: Header file with some defines for verification
// *****************************************************************************

`ifndef VF_UTILS
`define VF_UTILS

// *****************************************************************************

// Set of ports used by the Aica (DUT) processor
typedef struct {
  logic               clk;
  logic               rst;
  logic               irq;
  logic               irq_ack;
  logic               imem_wait;
  logic               imem_req;
  logic [XLEN-1:ILSB] imem_addr;
  logic [XLEN-1:0]    imem_data;
  logic               dmem_wait;
  logic               dmem_req;
  logic [3:0]         dmem_we_byte;
  logic [XLEN-1:0]    dmem_addr;
  logic [XLEN-1:0]    dmem_ram_data;
  logic [XLEN-1:0]    dmem_core_data;

} t_dut_ports;

// *****************************************************************************
`endif  //VF_UTILS
