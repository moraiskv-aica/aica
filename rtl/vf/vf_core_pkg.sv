// *****************************************************************************
//      Author: Kevin Paula Morais
//        Date: 07/11/2024
//        File: vf_core_pkg
// Description: Package with some constants and types used by the RISC-V core
//              reference
// *****************************************************************************

package vf_core_pkg;

  localparam XLEN   = 32; // register file depth
  localparam N_XLEN = 5;  // register file address size => 2^(N_XLEN) = XLEN
  localparam ILSB   = 1;  // instruciton LSBit used for aligned access

  // ***************
  // instruction valid bits => [1:0]
  localparam RV32_VALID_BITS = 2'b11;

  // instruction opcode => [6:2]
  localparam OPCODE_LUI    = 5'b01101;
  localparam OPCODE_AUIPC  = 5'b00101;
  localparam OPCODE_JAL    = 5'b11011;
  localparam OPCODE_JALR   = 5'b11001;
  localparam OPCODE_BRANCH = 5'b11000;
  localparam OPCODE_LOAD   = 5'b00000;
  localparam OPCODE_STORE  = 5'b01000;
  localparam OPCODE_OPIMM  = 5'b00100;
  localparam OPCODE_OPREG  = 5'b01100;
  localparam OPCODE_SYSTEM = 5'b11100;
  localparam OPCODE_CSR    = 5'b11100;  //same as SYSTEM

  // RISC-V NOP instruction
  localparam RV32I_NOP = 32'h00000013;

  // instructions fields (decoded)
  typedef struct packed {
    logic [XLEN-1:0]   raw;
    logic [N_XLEN-1:0] rs2;     // instr[24:20]
    logic [N_XLEN-1:0] rs1;     // instr[19:15]
    logic [14:12]      fct3;    // instr[14:12]
    logic [N_XLEN-1:0] rd;      // instr[11:07]
    logic [6:2]        opcode;  // instr[06:02]
    logic [N_XLEN-1:0] shamt;   // instr[24:20]
    logic [XLEN-1:0]   imm;     // immediate signal already decoded
  } t_riscv_instr;

  // simple pipeline register to be used as a shift register
  typedef struct packed {
  	logic[XLEN-1:0] pc;
  	t_riscv_instr   instr;
  } t_pipeline_regs;

  // pipeline info
  localparam N_STAGES  = 4;
  localparam STAGE_IF  = 1;
  localparam STAGE_ID  = 2;
  localparam STAGE_EX  = 3;
  localparam STAGE_WB  = 4;

  // fsm used to fetch instructions
  typedef enum {
    FSM_IFETCH_DIS,     // instruction fetch from memory is disabled, thus, imem_req_o and pc_we should also be
    FSM_IFETCH_START,   // instruction fetch is enabled, but we are inserting bubble at the pipeline (since it's empty)
    FSM_IFETCH_READY    // instruction fetch is enabled fully enabled (returns to FSM_IFETCH_START on flush)
  } t_fetch_fsm;

endpackage
