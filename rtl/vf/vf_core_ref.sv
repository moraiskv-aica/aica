// *****************************************************************************
// Author:      Kevin Paula Morais
// Date:        07/11/2024
// File:        vf_core_ref.sv
// Description: RISC-V Core used as reference for verification.
//    1) Emulates pipeline behavior like fetching instructions following a branch
//      (since the branch isn't executed immediately), so the outputs reflects the
//      real results for comparison;
//    2) Internally, the pipeline is only a shift register, which carries the
//      instruction (and it's PC) for N cycles until it arrives at the stage that
//      it's executed;
//    3) Most of the execution is done at stage 4 WB, leaving only the data memory
//      interface at stage 3 EX, otherwise the ports for this reference would
//      not be accurate;
// .
// *****************************************************************************

import vf_core_pkg::*;

// *****************************************************************************
module vf_core_ref #(
  parameter int N_DATA = 32,  // data size
  parameter int ILSB   = 1    // instruction LSBit (aligned address)
)(
  input  logic                 clk_i,
  input  logic                 rst_i,
  input  logic                 irq_i,
  output logic                 irq_ack_o,
  input  logic                 imem_wait_i,
  output logic                 imem_req_o,
  output logic [N_DATA-1:ILSB] imem_addr_o,
  input  logic [N_DATA-1:0]    imem_data_i,
  input  logic                 dmem_wait_i,
  output logic                 dmem_req_o,
  output logic [3:0]           dmem_we_byte_o,
  output logic [N_DATA-1:0]    dmem_addr_o,
  input  logic [N_DATA-1:0]    dmem_data_i,
  output logic [N_DATA-1:0]    dmem_data_o
);

  string instr_name;

  // shift registers used to emulate the behavior of a pipeline
  // Note: stage 1 does not have an input register
  t_pipeline_regs [N_STAGES:2] r_pipe;
  t_fetch_fsm                  r_ifetch_fsm;
  t_riscv_instr                if_instr;

  logic flush;
  logic branch_taken;

  // Stage 1 IF
  // pc
  logic [N_DATA-1:0] r_pc;
  logic [N_DATA-1:0] r_pc_instr;
  logic [N_DATA-1:0] pc_next;
  logic [N_DATA-1:0] pc_offset;
  logic              imem_req;

  // Stage 4 WB
  // instruction and it's fields
  int                imm_msb; //msb used for signal extension

  logic [N_DATA-1:0] pc_branch;

  // register file
  logic [2**N_XLEN-1:0][N_DATA-1:0] r_regs_file;
  logic                             regs_file_we;

  logic [N_DATA-1:0] data_rs1;
  logic [N_DATA-1:0] data_rs2;
  logic [N_DATA-1:0] data_rd;

  // alu
  logic [N_DATA-1:0] opr_b;
  logic [N_XLEN-1:0] opr_shamt;

  // ***********************************
  // I/O
  assign irq_ack_o   = '0;
  assign imem_req_o  = imem_req;
  assign imem_addr_o = r_pc[N_DATA-1:ILSB];


  // ***********************************
  // Pipeline Emulation
  always_ff@(posedge clk_i)
  begin
    if (rst_i | flush)
    begin
      for (int i=2; i<=N_STAGES; i++)
      begin
        r_pipe[i].pc    <= '0;
        r_pipe[i].instr <= if_instr;
      end
    end
    else if (r_ifetch_fsm == FSM_IFETCH_READY)
    begin
      // stage 1 -> 2
      // new data from imem
      r_pipe[STAGE_ID].pc    <= r_pc_instr;
      r_pipe[STAGE_ID].instr <= if_instr;

      // shift current pipeline state
      for (int i=3; i<=N_STAGES; i++)
        r_pipe[i] <= r_pipe[i-1];
    end
  end

  // fetch fsm
  always_ff@(posedge clk_i)
  begin
    if (rst_i)
      r_ifetch_fsm <= FSM_IFETCH_DIS;
    else
    begin
      // keep at rst state for 1 cycle, meanwhile some signals like disabled
      case (r_ifetch_fsm)
        FSM_IFETCH_DIS    : r_ifetch_fsm <= FSM_IFETCH_START;
        FSM_IFETCH_START  : r_ifetch_fsm <= FSM_IFETCH_READY;
        FSM_IFETCH_READY  :
        begin
          if (flush)
            r_ifetch_fsm <= FSM_IFETCH_START;
        end
      endcase
    end
  end

  // flush signal
  assign flush = branch_taken;

  // ***********************************
  // Instruction Decode
  assign if_instr.raw    = (rst_i | flush) ? RV32I_NOP : imem_data_i;
  assign if_instr.rs2    = if_instr.raw[24:20];
  assign if_instr.rs1    = if_instr.raw[19:15];
  assign if_instr.fct3   = if_instr.raw[14:12];
  assign if_instr.rd     = if_instr.raw[11:07];
  assign if_instr.opcode = if_instr.raw[06:02];
  assign if_instr.shamt  = if_instr.raw[24:20];

  // immediate decode
  always_comb
  begin
    imm_msb      = 1'b0;
    if_instr.imm = '0;

    case (if_instr.opcode)
      OPCODE_LUI,
      OPCODE_AUIPC  :
      begin
        if_instr.imm[31:12] = if_instr.raw[31:12];
        imm_msb             = 31;
      end
      OPCODE_JAL    :
      begin
        if_instr.imm[20:0]  = {if_instr.raw[31], if_instr.raw[19:12], if_instr.raw[20], if_instr.raw[30:21], 1'b0};
        imm_msb              = 20;
      end
      OPCODE_JALR,
      OPCODE_LOAD,
      OPCODE_OPIMM  :
      begin
        if_instr.imm[11:0] = if_instr.raw[31:20];
        imm_msb            = 11;
      end
      OPCODE_BRANCH :
      begin
        if_instr.imm[12:0] = {if_instr.raw[31], if_instr.raw[7], if_instr.raw[30:25], if_instr.raw[11:8], 1'b0};
        imm_msb            = 12;
      end
      OPCODE_STORE  :
      begin
        if_instr.imm[11:0] = {if_instr.raw[31:25], if_instr.raw[11:7]};
        imm_msb            = 11;
      end
      default       : if_instr.imm = '0;
    endcase

    for (int i=31; i>imm_msb; i--)
      if_instr.imm[i] = if_instr.imm[imm_msb];
  end

  // string for debug
  always_comb
  begin
    case (if_instr.opcode)
      OPCODE_SYSTEM : instr_name = "SYSTEM";
      OPCODE_LUI    : instr_name = "LUI___";
      OPCODE_AUIPC  : instr_name = "AUIPC_";
      OPCODE_JAL    : instr_name = "JAL___";
      OPCODE_JALR   : instr_name = "JALR__";
      OPCODE_BRANCH : instr_name = "BRANCH";
      OPCODE_LOAD   : instr_name = "LOAD__";
      OPCODE_STORE  : instr_name = "STORE_";
      OPCODE_OPIMM  : instr_name = "REGIMM";
      OPCODE_OPREG  : instr_name = "REGREG";
      default       :
      begin
        $display("[%m][ERROR] invalid instruction opcode : if_instr.raw=%X", if_instr.raw);
        instr_name = "___INV";
      end
    endcase
  end

  // ***********************************
  // STAGE 1 IF
  // ***********************************

  // PC increment
  always_ff @(posedge clk_i)
  begin
    if (rst_i)
    begin
      r_pc       <= '0;
      r_pc_instr <= '0;
    end
    else if (r_ifetch_fsm != FSM_IFETCH_DIS)
    begin
      r_pc       <= pc_next;
      r_pc_instr <= r_pc; //store PC used to fetch instruction
    end
  end

  assign imem_req = (r_ifetch_fsm != FSM_IFETCH_DIS) & !flush;

  // every possible PC input data
  assign pc_offset = r_pc+4;

  // mux for PC
  always_comb
  begin
    if (branch_taken)
      pc_next = pc_branch;
    else
      pc_next = pc_offset;
  end

  // ***********************************
  // STAGE 2 ID
  // ***********************************

  // This is stage is skipped, the instruction
  // is decoded directly at Stage 1 IF

  // ***********************************
  // STAGE 3 EX
  // ***********************************
  // This is stage is used only for memory access,
  // the actual operation (i.e., from ALU) is executed at
  // the next stage(s)

  assign dmem_req_o  = (r_pipe[STAGE_EX].instr.opcode == OPCODE_STORE) | (r_pipe[STAGE_EX].instr.opcode == OPCODE_LOAD);

  always_comb
  begin
    // this is the only part that needs forwarding, since the load instruction requires a 1 cycle latency
    // there isn't way to adjust it to be fully executed in a single cycle like the others

    // address
    if ( (r_pipe[STAGE_WB].instr.rd == r_pipe[STAGE_EX].instr.rs1) & regs_file_we )
      dmem_addr_o = data_rd + r_pipe[STAGE_EX].instr.imm;
    else
      dmem_addr_o = r_regs_file[r_pipe[STAGE_EX].instr.rs1] + r_pipe[STAGE_EX].instr.imm;

    // data
    if ( (r_pipe[STAGE_WB].instr.rd == r_pipe[STAGE_EX].instr.rs2) & regs_file_we )
      dmem_data_o = data_rd;
    else
      dmem_data_o = r_regs_file[r_pipe[STAGE_EX].instr.rs2];
  end

  // data memory: write byte enable
  always_comb
  begin
    if (r_pipe[STAGE_EX].instr.opcode == OPCODE_STORE)
    begin
      case (r_pipe[STAGE_EX].instr.fct3)
        3'b000   : dmem_we_byte_o = 4'b0001;
        3'b001   : dmem_we_byte_o = 4'b0011;
        3'b010   : dmem_we_byte_o = 4'b1111;
        default :
        begin
          $display("[%m] invalid funct3 value : r_pipe[STAGE_EX].instr=%X", r_pipe[STAGE_EX].instr);
          dmem_we_byte_o = 4'b0100;
        end
      endcase
    end
    else
      dmem_we_byte_o = 4'b0000;
  end

  // ***********************************
  // STAGE 4 WB
  // ***********************************

  // ***************
  // Branch logic
  always_comb
  begin
    if (r_pipe[STAGE_WB].instr.opcode == OPCODE_BRANCH)
    begin
      case (r_pipe[STAGE_WB].instr.fct3)
        3'b000  : branch_taken = data_rs1            == data_rs2;             // BEQ
        3'b001  : branch_taken = data_rs1            != data_rs2;             // BNE
        3'b100  : branch_taken = signed'(data_rs1)   <  signed'(data_rs2);    // BLT
        3'b101  : branch_taken = signed'(data_rs1)   >= signed'(data_rs2);    // BGE
        3'b110  : branch_taken = unsigned'(data_rs1) <  unsigned'(data_rs2);  // BLTU
        3'b111  : branch_taken = unsigned'(data_rs1) >= unsigned'(data_rs2);  // BGEU
        default :
        begin
          $display("[%m][ERROR] invalid funct3 from branch instructions : r_pipe[WB].instr=%X", r_pipe[STAGE_WB].instr.raw);
          branch_taken = 1'b0;
        end
      endcase
    end
    else if ( (r_pipe[STAGE_WB].instr.opcode == OPCODE_JAL) | (r_pipe[STAGE_WB].instr.opcode == OPCODE_JALR) )  //jump is always taken
      branch_taken = 1'b1;
    else
      branch_taken = 1'b0;
  end

  assign pc_branch = (r_pipe[STAGE_WB].instr.opcode == OPCODE_JALR) ? data_rs1+r_pipe[STAGE_WB].instr.imm : r_pipe[STAGE_WB].pc+r_pipe[STAGE_WB].instr.imm;

  // ***************
  // Register File Access
  // write data
  assign opr_b     = (r_pipe[STAGE_WB].instr.opcode == OPCODE_OPIMM) ? r_pipe[STAGE_WB].instr.imm         : data_rs2;
  assign opr_shamt = (r_pipe[STAGE_WB].instr.opcode == OPCODE_OPIMM) ? r_pipe[STAGE_WB].instr.shamt : data_rs2;

  always_comb
  begin
    data_rd = '0;
    case (r_pipe[STAGE_WB].instr.opcode)
      OPCODE_LUI    : data_rd = r_pipe[STAGE_WB].instr.imm;
      OPCODE_AUIPC  : data_rd = r_pipe[STAGE_WB].pc+r_pipe[STAGE_WB].instr.imm;
      OPCODE_JALR,
      OPCODE_JALR   : data_rd = r_pipe[STAGE_WB].pc+4;
      OPCODE_LOAD   :
      begin
        case (r_pipe[STAGE_WB].instr.fct3)
          "000"   : data_rd = {{24{dmem_data_i[7]}},  dmem_data_i[7:0]} ; // LB
          "001"   : data_rd = {{16{dmem_data_i[15]}}, dmem_data_i[15:0]}; // LH
          "010"   : data_rd = dmem_data_i;                                // LW
          "100"   : data_rd = {24'b0, dmem_data_i[7:0]};                  // LBU
          "101"   : data_rd = {16'b0, dmem_data_i[15:0]};                 // LHU
          default : data_rd = '0;
        endcase
      end
      OPCODE_OPREG,
      OPCODE_OPIMM  :
      begin
        // ALU
        case (r_pipe[STAGE_WB].instr.fct3)
          3'b000 :
          begin
            if ((r_pipe[STAGE_WB].instr.raw[30] == 1'b0) | (r_pipe[STAGE_WB].instr.fct3 == OPCODE_OPIMM))
              data_rd = data_rs1 + opr_b;
            else
              data_rd = data_rs1 - opr_b;
          end
          3'b001 : data_rd = data_rs1 << opr_shamt;
          3'b010 : data_rd[0] = signed'(data_rs1) < signed'(opr_b)     ? 1'b1 : 1'b0;
          3'b011 : data_rd[0] = unsigned'(data_rs1) < unsigned'(opr_b) ? 1'b1 : 1'b0;
          3'b100 : data_rd = data_rs1 ^ opr_b;
          3'b101 :
          begin
            if (r_pipe[STAGE_WB].instr.raw[30] == 1'b0)
              data_rd = data_rs1 >> opr_shamt;
            else
              data_rd = data_rs1 >>> opr_shamt;
          end
          3'b110 : data_rd = data_rs1 | opr_b;
          3'b111 : data_rd = data_rs1 & opr_b;
        endcase
      end
      default    : data_rd = '0; //shouldn't write
    endcase
  end

  // write enable
  always_comb
  begin
    case (r_pipe[STAGE_WB].instr.opcode)
    OPCODE_LUI,
    OPCODE_AUIPC,
    OPCODE_JAL,
    OPCODE_JALR,
    OPCODE_LOAD,
    OPCODE_OPIMM,
    OPCODE_OPREG  : regs_file_we = 1'b1;
    default       : regs_file_we = 1'b0;
    endcase 
  end

  // read
  assign data_rs1 = r_regs_file[r_pipe[STAGE_WB].instr.rs1];
  assign data_rs2 = r_regs_file[r_pipe[STAGE_WB].instr.rs2];

  // write
  always_ff @(posedge clk_i)
  begin
    if (rst_i)
    begin
      for (int i=0; i<(2**N_XLEN); i++)
        r_regs_file[i] = '0;
    end
    else if (regs_file_we & !flush)
      r_regs_file[r_pipe[STAGE_WB].instr.rd] <= data_rd;
  end

endmodule
