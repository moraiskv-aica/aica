// *****************************************************************************
// Author:      Kevin Paula Morais
// Date:        07/11/2024
// File:        vf_module.sv
// Description: Module used to generate control signals (such as clock an rst)
//              for the DUT and it's reference, while monitoring their ports for
//              validation.
// *****************************************************************************

`include "vf_utils.svh"

// *****************************************************************************
module vf_monitor
(
  output logic       clk_o,
  output logic       rst_o,
  input  t_dut_ports dut_ports_i,
  input  t_dut_ports ref_ports_i
);

  localparam time CLK_PERIOD = 20ns;  // 50 MHz

  // control signals
  logic clk = 1'b0;
  logic rst;

  // *******************************************************
  // Clock & Rst generator
  assign clk_o = clk;
  assign rst_o = rst;

  // External Clock
  always begin
    clk = ~clk;
    # (CLK_PERIOD/2);
  end

  // External Reset : Assert High
  initial begin
    rst = 1'b0;
    #75ns;
    forever begin
      rst = 1'b1;
      #75ns;
      rst = 1'b0;
      #10s;
    end
  end

endmodule
