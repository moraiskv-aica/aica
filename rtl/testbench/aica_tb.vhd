library ieee;
use ieee.std_logic_1164.all;

use work.core_pkg.all;

-- *****************************************************************************
entity aica_tb is
end entity aica_tb;

-- *****************************************************************************
architecture behavior of aica_tb is

  constant CLK_PERIOD : time := 20 ns;

  signal clk : std_logic := '0';
  signal rst : std_logic;

  -- system configuration
  constant N_RAM_ADDR  : integer := 26;

  -- interrupt
  signal irq_ack : std_logic;

  -- ram interface
  signal imem_wait     : std_logic;
  signal imem_req      : std_logic;
  signal imem_addr     : std_logic_vector(N_DATA-1 downto ILSB);
  signal imem_data     : std_logic_vector(N_DATA-1 downto 0);
  signal dmem_wait     : std_logic;
  signal dmem_req      : std_logic;
  signal dmem_we_byte  : std_logic_vector(3 downto 0);
  signal dmem_addr     : std_logic_vector(N_DATA-1 downto 0);
  signal dmem_ram_data : std_logic_vector(N_DATA-1 downto 0);
  signal dmem_core_data: std_logic_vector(N_DATA-1 downto 0);

  signal ram_addra : std_logic_vector(N_RAM_ADDR-1 downto 0);
  signal ram_addrb : std_logic_vector(N_RAM_ADDR-1 downto 0);

begin

  -- ***********************************
  -- External Clock : 50 MHz
  process
  begin
    loop
      clk <= not(clk); wait for CLK_PERIOD/2;
    end loop;
  end process;

  -- External Reset : Assert High
  process
  begin
    rst <= '0'; wait for 75 ns;
    loop
      rst <= '1'; wait for 75 ns;
      rst <= '0'; wait for 10 sec;
    end loop;
  end process;

  -- ***********************************
  ram_addra <= imem_addr(N_RAM_ADDR-1 downto ILSB)&"0";
  ram_addrb <= dmem_addr(N_RAM_ADDR-1 downto 0);

  -- ***********************************
  -- aica processor
  i_aica: entity work.aica_top
  generic map(
    USE_PREDICTOR => false
  )
  port map(
    clk_i           => clk,
    rst_i           => rst,
    irq_i           => '0',
    irq_ack_o       => irq_ack,
    imem_wait_i     => imem_wait,
    imem_req_o      => imem_req,
    imem_addr_o     => imem_addr,
    imem_data_i     => imem_data,
    dmem_wait_i     => dmem_wait,
    dmem_req_o      => dmem_req,
    dmem_we_byte_o  => dmem_we_byte,
    dmem_addr_o     => dmem_addr,
    dmem_data_i     => dmem_ram_data,
    dmem_data_o     => dmem_core_data
  );

  -- ***********************************
  -- external RAM
  i_ram_dp_tb: entity work.ram_dp_tb
  generic map(
    N_DATA   => N_DATA,
    N_ADDR   => N_RAM_ADDR,   -- ram size is set through the address size
    MEM_FILE => "test.mem"
  )
  port map(
    clk_i     => clk,
    rst_i     => rst,
    waita_o   => imem_wait,
    ena_i     => imem_req,
    addra_i   => ram_addra,
    douta_o   => imem_data,
    waitb_o   => dmem_wait,
    enb_i     => dmem_req,
    we_byte_i => dmem_we_byte,
    addrb_i   => ram_addrb,
    dinb_i    => dmem_core_data,
    doutb_o   => dmem_ram_data
  );


end architecture behavior;
