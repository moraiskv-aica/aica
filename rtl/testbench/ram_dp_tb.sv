// *****************************************************************************
// Author:      Kevin Paula Morais
// Date:        23/10/2024
// File:        ram_dp_tb.sv
// Description: Generic RAM implemented with two ports, porta A for read access
//              only, and port B with read/write access (not simultaneous). The
//              array is built of 2^N_ADDR words of 1 byte, so we can fill it's
//              content directly from a memory file generated from objcopy.
// *****************************************************************************

// *****************************************************************************
module ram_dp_tb #(
  parameter int    N_DATA     = 32,       // data size
  parameter int    N_ADDR     = 12,       // address size: mem size = 2^N_ADDR bytes
  parameter int    N_PIPELINE = 0,        // latency (in clk_i cycles) used to access the memory
  parameter string MEM_FILE   = "ram.mem" // path to file in disk used to load the memory data
)(
  input  logic              clk_i,        // clock input
  input  logic              rst_i,        // reset at 1
  // port A
  output logic              waita_o,      // wait port A access to be finished
  input  logic              ena_i,        // enable port A access
  input  logic [N_ADDR-1:0] addra_i,      // port A address
  output logic [N_DATA-1:0] douta_o,      // port A data output
  // port B
  output logic              waitb_o,      // wait port B access to be finished
  input  logic              enb_i,        // enable port B access
  input  logic [3:0]        we_byte_i,    // write enable with byte selection
  input  logic [N_ADDR-1:0] addrb_i,      // port B address
  input  logic [N_DATA-1:0] dinb_i,       // port B data input
  output logic [N_DATA-1:0] doutb_o       // port B data output
);

  // ***********************************
  // signals
  logic [7:0]        mem [2**N_ADDR-1:0];
  logic [N_DATA-1:0] r_douta;
  logic [N_DATA-1:0] r_doutb;
  logic [3:0]        byte_col;
  logic              ports_col;

  // ***********************************
  assign douta_o = r_douta;
  assign doutb_o = r_doutb;

  // Add latency to the RAM access
  generate
    if (N_PIPELINE == 0) begin
      assign waita_o = 1'b0;
      assign waitb_o = 1'b0;
    end
    else begin
      // TODO: Add N_PIPELINE feature
      assign waita_o = 1'b0;
      assign waitb_o = 1'b0;
    end
  endgenerate

  // ***********************************
  //  Collision detection (with byte enable)
  // ***********************************
  assign ports_col = ena_i && enb_i && (addra_i == addrb_i);

  genvar i;
  generate
    for (i = 0; i < 4; i = i + 1) begin : gen_byte_col
      assign byte_col[i] = ports_col && we_byte_i[i];
    end
  endgenerate

  // ***********************************
  //  Port A, read
  // ***********************************
  always_ff @(posedge clk_i)
  begin
    if (rst_i)
      r_douta <= '0;
    else if (ena_i == 1'b1) begin
      // Bypass bytes being written due to collision
      for (int i = 0; i < 4; i = i + 1) begin
        if (byte_col[i])
          r_douta[i*8 +: 8] <= dinb_i[i*8 +: 8];
        else
          r_douta[i*8 +: 8] <= mem[addra_i+i];
      end
    end
  end

  // ***********************************
  //  Port B, read/write with byte enable
  // ***********************************
  always_ff @(posedge clk_i) begin
    if (rst_i)
      r_doutb <= '0;
    else if (enb_i) begin
      if (we_byte_i != 4'b0000) begin
        // write operation with byte enables
        for (int i = 0; i<4; i++) begin
          if (we_byte_i[i])
            mem[addrb_i+i] <= dinb_i[i*8 +: 8];
        end
      end
      else  // read operation
        r_doutb <= {mem[addrb_i+3], mem[addrb_i+2], mem[addrb_i+1], mem[addrb_i]};
    end
  end

  // ***********************************
  // initialize ram with data from disk
  // ***********************************
  initial begin
    $display("[%m] loading RAM with data from disk: %s", MEM_FILE);
    $readmemh(MEM_FILE, mem);
  end
endmodule
