
AICA_TOOLS_DIR 				= ./submodules/aica-tools
AICA_TOOLS_TEST_DIR 	= $(AICA_TOOLS_DIR)/test
AICA_TOOLS_TEST_MEM 	= $(AICA_TOOLS_TEST_DIR)/build/test.mem
AICA_TOOLS_TEST_DUMP 	= $(AICA_TOOLS_TEST_DIR)/build/test.dump

# ******************************************************************************

# phony targets
.PHONY: sw-update

sw-update:
	make -C $(AICA_TOOLS_TEST_DIR) clean all &&\
	cp -fv $(AICA_TOOLS_TEST_MEM) $(AICA_TOOLS_TEST_DUMP) ./sw_bin
