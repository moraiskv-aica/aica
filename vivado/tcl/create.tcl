set FPGA_RTL rtl
set XILINX_PART     xc7a100tcsg324-1

set MEM_PATH        ../sw_bin
set RTL_SUBMODULES  ../submodules
set RTL_PATH        ../rtl
set RTL_CORE_PATH   $RTL_PATH/core
set RTL_TB_PATH     $RTL_PATH/testbench
set RTL_VF_PATH     $RTL_PATH/vf

set BUILD_DIR       build
set PROJECT_NAME    aica
set PROJECT_DIR     $BUILD_DIR/$PROJECT_NAME

set VERIF_SIM_SET   sim_vf

# **********************************************************
# Create Vivado Project
# **********************************************************
# create_project $PROJECT_NAME $PROJECT_DIR -force -part $XILINX_PART
create_project $PROJECT_NAME $PROJECT_DIR -force
# set_property board_part $XILINX_BOARD [current_project]

# Set Verilog Defines.
set DEFINES "PLATAFORM_FPGA=1"
set_property verilog_define $DEFINES [current_fileset]
set_property verilog_define $DEFINES [get_filesets sim_1]

# suppress warnings: "<parameter_name> becomes localparam in <module_name>"
# example commented:
# set_msg_config -id "Synth 8-11067" -suppress

# **********************************************************
# Add source files
# **********************************************************

# core
read_vhdl -vhdl2008 $RTL_CORE_PATH/alu_pkg.vhd
read_vhdl -vhdl2008 $RTL_CORE_PATH/alu.vhd
read_vhdl -vhdl2008 $RTL_CORE_PATH/register_file.vhd
read_vhdl -vhdl2008 $RTL_CORE_PATH/riscv_pkg.vhd
read_vhdl -vhdl2008 $RTL_CORE_PATH/core_pkg.vhd
read_vhdl -vhdl2008 $RTL_CORE_PATH/core_immediate.vhd
read_vhdl -vhdl2008 $RTL_CORE_PATH/core_instr_decoder.vhd
read_vhdl -vhdl2008 $RTL_CORE_PATH/core_csr.vhd
read_vhdl -vhdl2008 $RTL_CORE_PATH/core_stage1_if.vhd
read_vhdl -vhdl2008 $RTL_CORE_PATH/core_stage2_id.vhd
read_vhdl -vhdl2008 $RTL_CORE_PATH/core_stage3_ex.vhd
read_vhdl -vhdl2008 $RTL_CORE_PATH/core_stage4_wb.vhd
read_vhdl -vhdl2008 $RTL_CORE_PATH/core_top.vhd

# aica top files
read_vhdl -vhdl2008 $RTL_PATH/aica_top.vhd

# tb
read_verilog -sv $RTL_TB_PATH/ram_dp_tb.sv
read_vhdl -vhdl2008 $RTL_TB_PATH/aica_tb.vhd

# verification
read_verilog -sv $RTL_VF_PATH/vf_utils.svh
read_verilog -sv $RTL_VF_PATH/vf_core_pkg.sv
read_verilog -sv $RTL_VF_PATH/vf_core_ref.sv
read_verilog -sv $RTL_VF_PATH/vf_monitor.sv
read_verilog -sv $RTL_VF_PATH/vf_top.sv

# create another sim set (one for verification, other just to run the system testbench)
create_fileset -simset $VERIF_SIM_SET

# set top level
set_property top aica_tb [get_filesets sim_1];
set_property top vf_top  [get_filesets $VERIF_SIM_SET];

# disable unused files by the top level, and remove then from the project
# reorder_files -fileset sources_1 -auto -disable_unused -verbose
# remove_files -v [get_files -filter {!IS_ENABLED}]

# note: after reorder_files, otherwise it would be disabled
add_files -norecurse $MEM_PATH/test.mem

# Add constraints
add_files -fileset constrs_1 -norecurse aica.xdc

# **********************************************************
# do not close if running at gui mode
if {$rdi::mode != "gui"} {
  close_project
}
